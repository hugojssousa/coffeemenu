# Coffee Menu UI Theme

## Installation Instruction:

Clone the repository and run the command below

```
npm install or yarn
```

## Run app
### Mac
```
react-native start
```
and
```
react-native run-ios
```
Or
```
react-native run-android
```

Done!

### Linux
Run the emulator or connect your android device to the computer 
and run the command below

```
react-native start
```
and
```
react-native run-android
```

# To access the app

After running the application on your emulator or device, 
there are two types of pre-registered users, admin (admin@hotmail.com) 
and ordinary user (goj1@hotmail.com). 
Use one of these users or create others to test the app, 
but only regular users will be created. 
After login, test all the features you want.

:)

# Screenshots

## Common user
![Login](src/img/screenshots/1-login.png?raw=true)
![Login](src/img/screenshots/2-account.png?raw=true)
![Login](src/img/screenshots/commonuser-1.png?raw=true)
![Login](src/img/screenshots/commonuser-2.png?raw=true)
![Login](src/img/screenshots/commonuser-3.png?raw=true)
![Login](src/img/screenshots/commonuser-4.png?raw=true)
![Login](src/img/screenshots/commonuser-5.png?raw=true)
![Login](src/img/screenshots/commonuser-1.png?raw=true)
![Login](src/img/screenshots/commonuser-6.png?raw=true)
![Login](src/img/screenshots/commonuser-7.png?raw=true)
![Login](src/img/screenshots/commonuser-8.png?raw=true)
![Login](src/img/screenshots/commonuser-9.png?raw=true)
![Login](src/img/screenshots/commonuser-10.png?raw=true)
![Login](src/img/screenshots/commonuser-11.png?raw=true)

## Admin

![Login](src/img/screenshots/adminuser-1.png?raw=true)
![Login](src/img/screenshots/adminuser-2.png?raw=true)
![Login](src/img/screenshots/adminuser-3.png?raw=true)
![Login](src/img/screenshots/adminuser-4.png?raw=true)
![Login](src/img/screenshots/adminuser-5.png?raw=true)
![Login](src/img/screenshots/adminuser-6.png?raw=true)
![Login](src/img/screenshots/adminuser-7.png?raw=true)
![Login](src/img/screenshots/adminuser-8.png?raw=true)
![Login](src/img/screenshots/adminuser-9.png?raw=true)
![Login](src/img/screenshots/adminuser-10.png?raw=true)
![Login](src/img/screenshots/adminuser-11.png?raw=true)
