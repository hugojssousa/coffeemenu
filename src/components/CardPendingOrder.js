import React, {Component} from 'react';
import {Text, View} from 'react-native';
import appStyle from '../style/styles.app';
import {translate} from '../locales';
import {Button, Divider, Image} from 'react-native-elements';
import {connect} from 'react-redux';

class CardPendingOrder extends Component {

  getTotalPrice() {
    let total = 0.0

    this.props.items.map(i => {
      if (this.props.user.userAdmin) {
        total += (i.price * i.totalItemsAdminPending)
      } else {
        total += (i.price * i.totalItemsPending)
      }
    })

    return total
  }

  render() {
    return (
      <View style={[appStyle.styles.cardPedido, appStyle.styles.centerElements]}>
        <View style={[appStyle.styles.centerElements, appStyle.styles.viewTitlePedidos]}>
          <Text style={[appStyle.styles.titlePedidos2]}>{translate('orderPending1')} </Text>
          <Text style={[appStyle.styles.titlePedidos2]}>{translate('orderPending2')}</Text>
        </View>

        <View style={[appStyle.styles.viewCardItemPedido]}>
          {
            this.props.items.map(i => (
              <View key={i.id}>
                <View style={[appStyle.styles.cardItemPedido]}>
                  <Image source={i.img} style={[appStyle.styles.cardImg]}/>

                  <View style={[appStyle.styles.cardItemPedidoTitulo]}>
                    <Text style={[appStyle.styles.cardItemNome]}>
                      {this.props.user.userAdmin ? i.totalItemsAdminPending : i.totalItemsPending} {i.name}
                    </Text>
                    <Text style={[appStyle.styles.cardItemDescricao]}>{i.description}</Text>
                  </View>

                  <Text style={[appStyle.styles.cardItemValor]}>
                    {translate('money')} {(i.price * (this.props.user.userAdmin ? i.totalItemsAdminPending: i.totalItemsPending)).toFixed(2)}
                  </Text>
                </View>

                <Divider style={{backgroundColor: appStyle.color4}}/>
              </View>
            ))
          }
        </View>

        <View style={[appStyle.styles.cardTotalPedido, appStyle.styles.widthBody]}>
          <Text style={[appStyle.styles.cardTotal]}>{translate('total')}</Text>
          <Text style={[appStyle.styles.cardTotalValor]}>
            {translate('money')} {this.getTotalPrice().toFixed(2)}
          </Text>
        </View>

        <Button
          onPress={() => this.props.finishOrder()}
          buttonStyle={[appStyle.styles.btnCard]}
          title={translate('textBtnFinishOrder')}
          titleStyle={{color: appStyle.color11}}
        />

        <Button
          onPress={() => this.props.cancel()}
          buttonStyle={[appStyle.styles.btnCard]}
          title={translate('cancel')}
          titleStyle={{color: appStyle.color11}}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.users.filter((u) => u.logged)[0],
});

export default connect(mapStateToProps)(CardPendingOrder)
