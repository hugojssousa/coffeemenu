import React, {Component} from 'react';
import {Modal, SafeAreaView, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import appStyle from '../style/styles.app';
import {translate} from '../locales';
import {Button} from 'react-native-elements';
import {cancelTable} from '../redux/actions';

class CancelTableModal extends Component {

  constructor(props) {
    super(props)

    this.state = {
      totalItems: 0,
      orderModal: '',
      selectedTable: 0,
    }
  }

  render() {
    return (
      <View>
        <Modal
          onShow={() => this.setState({selectedTable: 0})}
          animationType="slide"
          transparent={true}
          visible={this.props.modalVisible()}>
          <SafeAreaView style={[appStyle.styles.container, appStyle.styles.viewModal]}>
            <View style={{
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: appStyle.color6,
              borderRadius: 10,
              width: appStyle.width * 0.9
            }}>
              <Text style={[appStyle.styles.tituloModal]}>
                {translate('titleModalCancel')}
              </Text>

              {
                this.props.table ?
                  <View style={[appStyle.styles.viewTitleTable]}>
                    <Text style={[appStyle.styles.titleTable]}>{this.props.table.number}</Text>
                  </View>
                :
                  <ScrollView
                    horizontal={true}
                  >
                    {
                      this.props.tables.map(t => (
                        <TouchableOpacity
                          key={t.number}
                          onPress={() => {
                            if (t.number === this.state.selectedTable) {
                              this.setState({selectedTable: 0})
                            } else {
                              this.setState({selectedTable: t.number})
                            }
                            this.forceUpdate()

                          }}
                          style={[appStyle.styles.tableModal, {backgroundColor: this.state.selectedTable === t.number ? appStyle.color2 : appStyle.color11}]}
                        >
                          <Text
                            style={[appStyle.styles.textTableModal, {color: this.state.selectedTable === t.number ? appStyle.color11 : appStyle.color2}]}>
                            {t.number}
                          </Text>
                        </TouchableOpacity>
                      ))
                    }
                  </ScrollView>
              }

              {
                this.props.table ?
                  <Button
                    onPress={() => {
                      this.props.cancelTable()
                      this.props.closeModal()
                    }}
                    buttonStyle={{
                      width: appStyle.width * 0.9,
                      backgroundColor: appStyle.color2,
                      borderRadius: 50,
                      marginVertical: 5
                    }}
                    title={translate('textLogoutTable')}
                    titleStyle={{color: appStyle.color11,}}
                  />
                :
                  <Button
                    onPress={() => {
                      this.props.setTable(this.state.selectedTable)
                      this.props.closeModal()
                    }}
                    buttonStyle={{
                      width: appStyle.width * 0.9,
                      backgroundColor: appStyle.color2,
                      borderRadius: 50,
                      marginVertical: 5
                    }}
                    title={translate('textOpenTable')}
                    titleStyle={{color: appStyle.color11,}}
                  />
              }

              <Button
                onPress={() => {
                  this.props.closeModal()
                }}
                buttonStyle={{
                  width: appStyle.width * 0.9,
                  backgroundColor: appStyle.color2,
                  borderRadius: 50,
                  marginVertical: 5
                }}
                title={translate('txtBtnOk')}
                titleStyle={{color: appStyle.color11,}}
              />
            </View>
          </SafeAreaView>
        </Modal>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  tables: state.tables,
  table: state.tables.filter(t => t.status === 'O')[0],
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  cancelTable: () => dispatch(cancelTable()),
});

export default connect(mapStateToProps, mapDispatchToProps)(CancelTableModal);
