import React, {Component} from 'react';
import {Text, View} from 'react-native';
import appStyle from '../style/styles.app';
import {translate} from '../locales';
import {Button, Divider, Image} from 'react-native-elements';
import {connect} from 'react-redux';

class CardConsumption extends Component {

  getTotalPrice() {
    let total = 0.0

    this.props.items.map(i => {
      if (this.props.user.userAdmin) {
        total += (i.price * i.totalItemsAdminSended)
      } else {
        total += (i.price * i.totalItemsSended)
      }
    })

    return total
  }

  render() {
    return (
      <View style={[appStyle.styles.cardPedido, appStyle.styles.centerElements]}>
        <View style={[appStyle.styles.centerElements, appStyle.styles.viewTitlePedidos]}>
          <Text style={[appStyle.styles.titlePedidos2]}>{translate('consumption1')} </Text>
          <Text style={[appStyle.styles.titlePedidos2]}>{translate('consumption2')}</Text>
        </View>

        {
          this.props.items.length > 0 ?
            this.props.items.map(i => (
              <View key={i.id}>
                <View style={[appStyle.styles.cardItemPedido]}>
                  <Image source={i.img} style={[appStyle.styles.cardImg]}/>

                  <View style={[appStyle.styles.cardItemPedidoTitulo]}>
                    <Text style={[appStyle.styles.cardItemNome]}>
                      {this.props.user.userAdmin ? i.totalItemsAdminSended : i.totalItemsSended} {i.name}
                    </Text>
                    <Text style={[appStyle.styles.cardItemDescricao]}>{i.description}</Text>
                  </View>

                  <Text style={[appStyle.styles.cardItemValor]}>
                    {translate('money')} {(i.price * (this.props.user.userAdmin ? i.totalItemsAdminSended: i.totalItemsSended)).toFixed(2)}
                  </Text>
                </View>

                <Divider style={{backgroundColor: appStyle.color4}}/>
              </View>
            ))
          :
            null
        }

        <View style={[appStyle.styles.cardTotalPedido, appStyle.styles.widthBody]}>
          <Text style={[appStyle.styles.cardTotal]}>Total</Text>
          <Text style={[appStyle.styles.cardTotalValor]}>{translate('money')} {this.getTotalPrice().toFixed(2)}</Text>
        </View>

        <Button
          onPress={() => this.props.closeConsumption()}
          buttonStyle={{
            width: appStyle.width * 0.8,
            backgroundColor: appStyle.color2,
            borderRadius: 50,
          }}
          title={translate('closeAccount')}
          titleStyle={{color: appStyle.color11}}
        />
      </View>

    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.users.filter((u) => u.logged)[0],
  }
}

export default connect(mapStateToProps)(CardConsumption)
