import React, {Component} from 'react';
import {Alert, SafeAreaView, ScrollView, Text, View} from 'react-native';
import appStyle from '../style/styles.app';
import {translate} from '../locales';
import CardPendingOrder from './CardPendingOrder';
import CardConsumption from './CardConsumption';
import {connect} from 'react-redux';

class TabOrders extends Component {
  constructor(props) {
    super(props)

    this.state = {
      orderWithItems: false
    }
  }

  finishOrder() {

    let message = ''
    let totalPriceItems = 0

    this.props.table.orders.map((o) => {
      if (o.status === 'P') {
        o.items.map(i => {
          message += i.totalItemsPending + ' ' + i.name + '\n'
        })
      }
    })

    this.props.table.orders.map((o) => {
      if (o.status === 'P') {
        o.items.map(i => {
          if (i.totalItemsPending > 0) {
            totalPriceItems += i.price * i.totalItemsPending
          }
        })
      }
    })

    message += translate('money') + ' ' + totalPriceItems.toFixed(2)

    Alert.alert(
      translate('textAlertConfirmOrder'),
      message,
      [
        {
          text: translate('textAlertCancelItem'),
          onPress: () => {
          },
          style: 'cancel',
        },
        {
          text: translate('textAlertOkItem'),
          onPress: () => {
            this.props.table.orders.map((o) => {
              if (o.status === 'P') {
                o.status = 'S'
              }
            })

            this.props.categories.map(c => {
              c.items.map(i => {
                i.totalItemsSended = i.totalItemsPending
                i.totalItemsPending = 0
              })
            })
            this.forceUpdate()
          },
        },
      ],
      {cancelable: false},
    );
  }

  closeConsumption() {
    Alert.alert(
      translate('closeAccount'),
      'We are grateful for your preference.\n',
      [
        {
          text: translate('txtBtnOk'),
          onPress: () => {
            this.props.categories.map(c => {
              c.items.map(i => {
                i.totalItemsSended = 0
              })
            })
            this.props.table.orders = []
            this.forceUpdate()
          },
          style: 'ok',
        },
      ],
      {cancelable: false},
    );
  }

  cancelPending() {
    Alert.alert(
      translate('closeAccount'),
      'Are you sure you want to cancel?\n',
      [
        {
          text: translate('txtBtnOk'),
          onPress: () => {
            let newOrders = this.props.table.orders.filter(o => o.status === 'S')

            this.props.table.orders = newOrders

            this.props.categories.map(c => {
              c.items.map(i => {
                i.totalItemsSended = 0
              })
            })

            this.forceUpdate()
          },
          style: 'ok',
        },
      ],
      {cancelable: false},
    );
  }

  componentDidMount(): void {
  }

  render() {
    return (
      <SafeAreaView>
        <ScrollView>
          <View style={[appStyle.styles.centerElements]}>
            {
              this.props.table && this.props.table.status ===  'O' && this.props.table.orders.length === 0 ?
                <Text style={[appStyle.textColorDefault, appStyle.styles.textNoOrder]}>{translate('noOrder')}</Text>
                :
                null
            }

            {
              !this.props.table ?
                <Text style={[appStyle.textColorDefault, appStyle.styles.textNoOrder]}>{translate('noTableOpened')}</Text>
                :
                null
            }

            {
              this.props.table && this.props.table.status === 'O' && this.props.table.orders.length > 0 ?
                (
                  this.props.table.orders.map((o) => {
                    if (o.status === 'P') {
                      return (
                        <CardPendingOrder
                          key={o.items.length}
                          items={o.items}
                          finishOrder={() => this.finishOrder()}
                          cancel={() => this.cancelPending()}
                        />
                      )
                    }
                  })
                )
                :
                null
            }

            {
              this.props.table && this.props.table.status === 'O' && this.props.table.orders.length > 0 ?
                (
                  this.props.table.orders.map((o) => {
                    if (o.status === 'S') {
                      return (
                        <CardConsumption
                          key={o.id}
                          items={o.items}
                          closeConsumption={() => this.closeConsumption()}
                        />
                      )
                    }
                  })
                )
                :
                null
            }
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => ({
  categories: state.menu.categories,
  table: state.tables.filter(t => t.status === 'O')[0],
})

const mapDispatchToProps = (dispatch) => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(TabOrders)
