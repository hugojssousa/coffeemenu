import React, {Component} from 'react';
import {Text, View} from 'react-native';
import appStyle from '../style/styles.app';
import {translate} from '../locales';
import {Button, Divider, Image} from 'react-native-elements';
import {connect} from 'react-redux';

class CardTable extends Component {

  constructor(props) {
    super(props)
    this.state = {
      totalPrice: 0.0
    }
  }

  componentDidMount(): void {
    let total = 0.0
    this.props.orders.map(o => {
      o.items.map(i => {
        total += (i.price * i.totalItemsSended)
      })
    })
    this.setState({totalPrice: total})
  }

  render() {
    const {orders} = this.props
    return (
      <View>
        {
          <View style={[appStyle.styles.cardPedido, appStyle.styles.centerElements]}>
            <View style={[appStyle.styles.centerElements, appStyle.styles.viewTitlePedidos]}>
              <Text style={[appStyle.styles.titlePedidos2]}>{translate('consumption1')} </Text>
              <Text style={[appStyle.styles.titlePedidos2]}>{translate('consumption2')}</Text>
            </View>

            <View style={[appStyle.styles.viewCardItemPedido]}>
              {
                orders.map(o => (
                  o.items.map(i => (
                    <View key={i.id}>
                      <View style={[appStyle.styles.cardItemPedido]}>
                        <Image source={i.img} style={[appStyle.styles.cardImg]}/>

                        <View style={[appStyle.styles.cardItemPedidoTitulo]}>
                          <Text style={[appStyle.styles.cardItemNome]}>{i.totalItemsSended} {i.name}</Text>
                          <Text style={[appStyle.styles.cardItemDescricao]}>{i.description}</Text>
                        </View>

                        <Text style={[appStyle.styles.cardItemValor]}>{translate('money')} {(i.price * i.totalItemsSended).toFixed(2)}</Text>
                      </View>

                      <Divider style={{backgroundColor: appStyle.color4}}/>
                    </View>
                  ))
                ))
              }
            </View>

            <Divider style={{backgroundColor: appStyle.color4}}/>

            <View style={[appStyle.styles.cardTotalPedido, appStyle.styles.widthBody]}>
              <Text style={[appStyle.styles.cardTotal]}>{translate('total')}</Text>
              <Text style={[appStyle.styles.cardTotalValor]}>
                {translate('money')} {this.state.totalPrice.toFixed(2)}
              </Text>
            </View>

            <Button
              onPress={() => this.props.closeConsumption()}
              buttonStyle={{
                width: appStyle.width * 0.8,
                backgroundColor: appStyle.color2,
                borderRadius: 50,
              }}
              title={translate('closeAccount')}
              titleStyle={{color: appStyle.color11}}
            />
          </View>
        }
      </View>
    );
  }
}

const mapStateToProps = (state) => ({})

const mapDispatchToProps = (dispatch) => ({})

export default connect(mapStateToProps)(CardTable)
