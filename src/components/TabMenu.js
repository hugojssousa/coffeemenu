import React, {Component} from 'react';
import {ActivityIndicator, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {Image} from 'react-native-elements';
import appStyle from '../style/styles.app';
import {connect} from 'react-redux';

class TabMenu extends Component {
  render() {
    return (
      <ScrollView>
        <View style={appStyle.styles.cards}>

          {
            this.props.categoryActives.map((c, k) => (
              <TouchableOpacity
                key={k}
                style={appStyle.styles.card}
                onPress={() => this.props.goToItem(c)}>
                <Image
                  PlaceholderContent={<ActivityIndicator/>}
                  source={c.img} style={{width: 90, height: 90, resizeMode: 'contain'}}/>
                <Text style={[appStyle.styles.textoCard, appStyle.styles.marginTop]}>{c.name}</Text>
              </TouchableOpacity>
            ))
          }

        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = (state) => ({
  categoryActives: state.menu.categories.filter((c) => c.active),
});

const mapDispatchToProps = ( dispatch) => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(TabMenu)
