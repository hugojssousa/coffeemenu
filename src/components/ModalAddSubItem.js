/* @flow */

import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {Icon} from 'react-native-elements';
import Modal from 'react-native-modal';
import appStyle from '../style/styles.app';

export default class ModalAddSubItem extends Component {

  constructor(props) {
    super(props)
    this.state = {

    }
  }

  getIconBackHeader() {
    return (
      <Icon
        name='ios-close'
        type='ionicon' color={appStyle.color11}
        onPress={() => {this.props.setVisible()}}/>
    )
  }

  render() {
    return (
        <Modal
          isVisible={this.props.visible}
          style={[appStyle.styles.modalAddItem]}>
          <View style={[appStyle.styles.viewModalAddItem]}>
            <View>
              {this.getIconBackHeader()}
              <Text>Add items</Text>
            </View>
            <View>
              <Text>Body modal</Text>
            </View>
          </View>
        </Modal>
    );
  }
}
