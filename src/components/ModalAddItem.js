/* @flow */

import React, {Component} from 'react';
import {ActivityIndicator, Alert, KeyboardAvoidingView, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {Button, Icon, Image, Input} from 'react-native-elements';
import Modal from 'react-native-modal';
import ImagePicker from 'react-native-image-picker';
import NumericInput from '@wwdrew/react-native-numeric-textinput';
import appStyle from '../style/styles.app';

export default class ModalAddItem extends Component {

  constructor(props) {
    super(props)

    this.state = {
      visibleModalItem: false,
      itemSelected: this.props.itemSelected,
      item: {
        id: 0,
        type: '',
        name: '',
        size: '',
        description:'',
        price: 0.0,
        img: '',
        totalItemsPending: 0,
        totalItemsSended: 0,
        totalItemsAdminPending: 0,
        totalItemsAdminSending: 0
      }
    }
  }

  getIconBackHeader() {
    return (
      <Icon
        name='ios-close'
        type='ionicon' color={appStyle.color11}
        onPress={() => {this.props.setModalVisible()}}/>
    )
  }

  selecionarAvatar() {
    const options = {
      title: 'Select the image item',
      storageOptions: {
        skipBackup: true,
        path: 'images',
        cameraRoll: true,
        waitUntilSaved: true
      },
    };

    ImagePicker.launchImageLibrary(options, (response) => {
      const source = {uri: 'data:' + response.type + ';base64,' + response.data};

      if (source.uri !== '' || source.uri) {
        this.setState({item: {...this.state.item, img: source}})
      }
    })
  }

  render() {
    return (
        <Modal
          isVisible={this.props.visible}
          style={[appStyle.styles.modalAddItem]}>
          <KeyboardAvoidingView
            behavior="padding" enabled>
          <View style={[appStyle.styles.viewModalAddItem]}>
            <View style={[appStyle.styles.titleModal]}>
              <Text style={[appStyle.styles.textTitleModal]}>
                {
                  this.props.itemSelected && this.props.itemSelected.type !== '' ?
                    <Text style={{color: appStyle.color11, fontWeight: '600', fontSize: 26}}>Edit {this.props.itemSelected.type}</Text>
                  :
                    <Text style={{color: appStyle.color11, fontWeight: '600', fontSize: 26}}>Create {this.state.item.type}</Text>
                }
              </Text>
            </View>

            <ScrollView style={[appStyle.styles.modalBody]}>
              <TouchableOpacity
                style={{justifyContent: 'center', alignItems: 'center', marginBottom: 10}}
                onPress={() => {
                  this.selecionarAvatar()
                }}>
                {
                  this.props.itemSelected && this.props.itemSelected.img !== '' ?
                    <Image
                      PlaceholderContent={<ActivityIndicator/>}
                      source={this.props.itemSelected.img} style={[appStyle.styles.imageEditItem]}/>
                  :
                    this.state.item && this.state.item.img === '' ?
                      <Icon
                        name="md-images"
                        type={'ionicon'}
                        size={110}
                        color={appStyle.color11}
                        iconStyle={{backgroundColor: appStyle.styles.color10}}
                        />
                    :
                      <Image
                        PlaceholderContent={<ActivityIndicator/>}
                        source={this.state.item.img} style={[appStyle.styles.imageEditItem]}/>
                }
                <Text>Tap here to choose new avatar</Text>
              </TouchableOpacity>

                <Input
                  containerStyle={[appStyle.styles.marginVertical, appStyle.styles.inputAccount, {backgroundColor: appStyle.color12}]}
                  inputContainerStyle={[{borderBottomWidth: 0}]}
                  inputStyle={{paddingLeft: 10}}
                  placeholder={'Type'}
                  editable={false}
                  value={this.props.itemSelected ? this.props.itemSelected.type : this.state.item.type}
                  />

                <Input
                  containerStyle={[appStyle.styles.marginVertical, appStyle.styles.inputAccount]}
                  inputContainerStyle={[{borderBottomWidth: 0,}]}
                  inputStyle={{paddingLeft: 10}}
                  placeholder={'Name'}
                  onChangeText={(text) => {
                    if (this.props.itemSelected) {
                      this.setState({itemSelected: {...this.state.itemSelected, name: text}})
                    } else {
                      this.setState({item: {...this.state.item, name: text}})
                    }
                  }}
                  value={this.state.itemSelected ? this.state.itemSelected.name : this.state.item.name}
                  />

                <Input
                  containerStyle={[appStyle.styles.marginVertical, appStyle.styles.inputAccount]}
                  inputContainerStyle={[{borderBottomWidth: 0,}]}
                  inputStyle={{paddingLeft: 10}}
                  placeholder={'Description'}
                  onChangeText={(text) => {
                    if (this.props.itemSelected) {
                      this.setState({itemSelected: {...this.state.itemSelected, description: text}})
                    } else {
                      this.setState({item: {...this.state.item, description: text}})
                    }
                  }}
                  value={this.props.itemSelected ? this.state.itemSelected.description : this.state.item.description}
                  />

                <Input
                  containerStyle={[appStyle.styles.marginVertical, appStyle.styles.inputAccount]}
                  inputContainerStyle={[{borderBottomWidth: 0,}]}
                  inputStyle={{paddingLeft: 10}}
                  placeholder={'Size'}
                  onChangeText={(text) => {
                    if (this.props.itemSelected) {
                      this.setState({itemSelected: {...this.state.itemSelected, size: text}})
                    } else {
                      this.setState({item: {...this.state.item, size: text}})
                    }
                  }}
                  value={this.props.itemSelected ? this.state.itemSelected.size : this.state.item.size}
                  />

                <NumericInput
                  style={[
                    appStyle.styles.marginVertical,
                    appStyle.styles.inputAccount,
                    {paddingLeft: 20, height: 40, fontSize: 18}]}
                  type='decimal'
                  decimalPlaces={2}
                  onUpdate={(text) => {
                    if (this.props.itemSelected) {
                      this.setState({itemSelected: {...this.state.itemSelected, price: text}})
                    } else {
                      this.setState({item: {...this.state.item, price: text}})
                    }
                  }}
                  value={this.state.itemSelected ? this.state.itemSelected.price : this.state.item.price}
                  />

            </ScrollView>

            <Button
              buttonStyle={{
                width: appStyle.width * 0.8,
                backgroundColor: appStyle.color2,
                borderRadius: 50,
                marginVertical: 5
              }}
              title="Add item"
              titleStyle={{color: appStyle.color11,}}
              disabled={this.state.item.name === '' || this.state.item.description === ''}
              onPress={() => {
                this.props.addItem(this.state.item)

                Alert.alert(
                  'Message',
                  'Item added',
                  [
                    {text: 'OK'},
                  ],
                  {cancelable: false},
                )

                this.setState({
                  item: {
                    id: '',
                    type: '',
                    name: '',
                    size: '',
                    description:'',
                    price: 0.0,
                    img: '',
                    totalItemsPending: 0,
                    totalItemsSended: 0,
                    totalItemsAdminPending: 0,
                    totalItemsAdminSending: 0
                  }
                })
              }}
            />

            <Button
              buttonStyle={{
                width: appStyle.width * 0.8,
                backgroundColor: appStyle.color2,
                borderRadius: 50,
                marginVertical: 5
              }}
              title="Ok"
              titleStyle={{color: appStyle.color11,}}
              onPress={() => {
                if (this.state.item.name !== '' && this.state.item.description !== '' && this.state.item.size !== '') {
                  this.props.addItem(this.state.itemSelected)
                  this.props.setModalVisible()
                } else {
                  this.props.setModalVisible()
                }
                this.setState({item: {
                  id: '',
                  type: '',
                  name: '',
                  size: '',
                  description:'',
                  price: 0.0,
                  img: '',
                  totalItemsPending: 0,
                  totalItemsSended: 0,
                  totalItemsAdminPending: 0,
                  totalItemsAdminSending: 0
                }})
              }}
            />
          </View>
          </KeyboardAvoidingView>
        </Modal>
    )
  }
}
