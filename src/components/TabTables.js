import React, {Component} from 'react';
import {SafeAreaView, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import appStyle from '../style/styles.app';
import {setSelectedTable} from '../redux/actions';

class TabTables extends Component {

  constructor(props) {
    super(props)
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1, width: appStyle.width, height: appStyle.height}}>
        <ScrollView>
          <View style={appStyle.styles.cards}>

            {
              this.props.tables.map(t => (
                <TouchableOpacity
                  key={t.number}
                  style={[appStyle.styles.cardTable]}
                  onPress={() => {
                    this.props.setSelectedTable(t)
                    this.props.goToTable()
                  }}>
                  <Text style={[appStyle.styles.textCardTable]}>{t.number}</Text>
                </TouchableOpacity>
              ))
            }

          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.users.filter((u) => u.logged)[0],
  tables: state.tables
})

const mapDispatchToProps = (dispatch) => ({
  setSelectedTable: (table) => dispatch(setSelectedTable(table)),
})

export default connect(mapStateToProps, mapDispatchToProps)(TabTables)
