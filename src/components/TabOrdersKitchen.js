import React, {Component} from 'react';
import {ScrollView, Text, View} from 'react-native';
import appStyle from '../style/styles.app';
import {Divider, Image} from 'react-native-elements';
import {connect} from 'react-redux';
import {translate} from '../locales';

class TabOrdersKitchen extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <ScrollView>
        <View style={[appStyle.styles.centerElements]}>
          <View style={[appStyle.styles.cardPedido, appStyle.styles.centerElements]}>
            <View style={[appStyle.styles.centerElements, appStyle.styles.viewTitlePedidos]}>
              <Text style={[appStyle.styles.titlePedidos2]}>{translate('orderPending1')} </Text>
              <Text style={[appStyle.styles.titlePedidos1]}>{translate('orderPending2')}</Text>
            </View>

            <View style={[appStyle.styles.viewCardItemPedido]}>
              {
                this.state.itensPedido.map(i => (
                  <View key={i.id}>
                    <View style={[appStyle.styles.cardItemPedido]}>
                      <Image source={i.img} style={[appStyle.styles.cardImg]}/>

                      <View style={[appStyle.styles.cardItemPedidoTitulo]}>
                        <Text style={[appStyle.styles.cardItemNome]}>{i.qtd} {i.nome}</Text>
                        <Text style={[appStyle.styles.cardItemDescricao]}>{i.descricao}</Text>
                      </View>

                      <Text style={[appStyle.styles.cardItemValor]}>US$ {i.valor}</Text>
                    </View>

                    <Divider style={{backgroundColor: appStyle.color4}}/>
                  </View>
                ))
              }
            </View>

            <View style={[appStyle.styles.cardTotalPedido, appStyle.styles.widthBody]}>
              <Text style={[appStyle.styles.cardTotal]}>{translate('total')}</Text>
              <Text style={[appStyle.styles.cardTotalValor]}>US$ 67,70</Text>
            </View>

          </View>


          <View style={[appStyle.styles.cardPedido, appStyle.styles.centerElements]}>
            <View style={[appStyle.styles.centerElements, appStyle.styles.viewTitlePedidos]}>
              <Text style={[appStyle.styles.titlePedidos2]}>{translate('orderCompleted1')} </Text>
              <Text style={[appStyle.styles.titlePedidos1]}>{translate('orderCompleted2')}</Text>
            </View>

            <View style={[appStyle.styles.viewCardItemPedido]}>
              {
                this.props.tables.map(i => (
                  <View key={i.id}>
                    <View style={[appStyle.styles.cardItemPedido]}>
                      <Image source={i.img} style={[appStyle.styles.cardImg]}/>

                      <View style={[appStyle.styles.cardItemPedidoTitulo]}>
                        <Text style={[appStyle.styles.cardItemNome]}>{i.qtd} {i.nome}</Text>
                        <Text style={[appStyle.styles.cardItemDescricao]}>{i.descricao}</Text>
                      </View>

                      <Text style={[appStyle.styles.cardItemValor]}>US$ {i.valor}</Text>
                    </View>

                    <Divider style={{backgroundColor: appStyle.color4}}/>
                  </View>
                ))
              }
            </View>

            <View style={[appStyle.styles.cardTotalPedido, appStyle.styles.widthBody]}>
              <Text style={[appStyle.styles.cardTotal]}>{translate('total')}</Text>
              <Text style={[appStyle.styles.cardTotalValor]}>US$ 67.60</Text>
            </View>
          </View>

          <View style={[appStyle.styles.cardPedido, appStyle.styles.centerElements]}>
            <View style={[appStyle.styles.centerElements, appStyle.styles.viewTitlePedidos]}>
              <Text style={[appStyle.styles.titlePedidos2]}>{translate('consumption1')} </Text>
              <Text style={[appStyle.styles.titlePedidos1]}>{translate('consumption2')}</Text>
            </View>

            <View style={[appStyle.styles.cardTotalPedido, appStyle.styles.widthBody]}>
              <Text style={[appStyle.styles.cardTotal]}>Total</Text>
              <Text style={[appStyle.styles.cardTotalValor]}>US$ 67.60</Text>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = (state) => ({
  tables: state.tables
})

export default connect(mapStateToProps)(TabOrdersKitchen)
