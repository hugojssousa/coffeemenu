import React, {Component} from 'react';
import {View} from 'react-native';
import {Button} from 'react-native-elements';
import appStyle from '../style/styles.app';
import {translate} from '../locales';

export default class BtnFinishOrder extends Component {

  constructor(props) {
    super(props)
    this.state = {
      modalBotaoAddPedidoVisible: true
    }
  }

  setModalVisible(visible) {
    this.setState({modalBotaoAddPedidoVisible: visible})
  }

  render() {
    return (
      <View>
        {
          this.props.showButton ?
            <Button
              onPress={() => this.props.pedir()}
              buttonStyle={{
                width: appStyle.width * 0.8,
                backgroundColor: appStyle.color2,
                borderRadius: 50,
              }}
              title={translate('textBtnFinishOrder')}
              titleStyle={{color: appStyle.color11}}
            />
            :
              null
        }
      </View>
    );
  }
}
