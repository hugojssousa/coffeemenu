import React, {Component} from 'react';
import {Modal, SafeAreaView, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import appStyle from '../style/styles.app';
import {translate} from '../locales';
import {Button} from 'react-native-elements';
import {clearItemsPendent, confirmeItems, sendOrder} from '../redux/actions';

class OrderTableModal extends Component {

  constructor(props) {
    super(props)

    this.state = {
      totalItems: 0,
      orderModal: '',
      selectedTable: 0,
      items: [],
    }
  }

  render() {
    return (
      <View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.props.modalVisible()}>
          <SafeAreaView style={[appStyle.styles.container, appStyle.styles.viewModal]}>
            <View style={{
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: appStyle.color6,
              borderRadius: 10,
              width: appStyle.width * 0.9
            }}>
              {
                !this.props.user.userAdmin ?
                  <View style={[appStyle.styles.titleWithTableModal]}>
                    <Text style={[appStyle.styles.titleTableModal]}>{translate('titleTable')} </Text>

                    <View style={[appStyle.styles.viewNumberTableModal]}>
                      <Text style={[appStyle.styles.numberTableModal]}>{this.props.table.number}</Text>
                    </View>
                  </View>
                :
                  null
              }

              <Text style={[appStyle.styles.tituloModal]}>{translate('modalOrderTitle')}</Text>

              <View style={[appStyle.styles.ordersModal]}>
                <Text style={[appStyle.styles.textOrderModal]}>{this.props.orders}</Text>
              </View>

              {
                this.props.user.userAdmin ?
                  <ScrollView
                    horizontal={true}
                  >
                    {
                      this.props.tables.map((t, k) => (
                        <TouchableOpacity
                          key={k}
                          onPress={() => {
                            if (t.number === this.state.selectedTable) {
                              this.setState({selectedTable: 0})
                            } else {
                              this.setState({selectedTable: t.number})
                            }
                            this.forceUpdate()

                          }}
                          style={[appStyle.styles.tableModal, {backgroundColor: this.state.selectedTable === t.number ? appStyle.color2 : appStyle.color11}]}
                        >
                          <Text
                            style={[appStyle.styles.textTableModal, {color: this.state.selectedTable === t.number ? appStyle.color11 : appStyle.color2}]}>
                            {t.number}
                          </Text>
                        </TouchableOpacity>
                      ))
                    }
                  </ScrollView>
                :
                  null
              }

              <Button
                onPress={() => {
                  this.props.confirmeItems(this.state.selectedTable)

                  if (this.props.user.userAdmin) {
                    this.props.sendOrder()
                  } else {
                    this.props.getTotalPending()
                  }

                  this.props.clearLocalItems()
                  this.props.navigate()
                }}
                buttonStyle={{
                  width: appStyle.width * 0.9,
                  backgroundColor: appStyle.color2,
                  borderRadius: 50,
                  marginVertical: 5
                }}
                title={translate('textGetOrder')}
                titleStyle={{color: appStyle.color11,}}
              />

              <Button
                onPress={() => {
                  this.props.closeModal()
                }}
                buttonStyle={{
                  width: appStyle.width * 0.9,
                  backgroundColor: appStyle.color2,
                  borderRadius: 50,
                  marginVertical: 5
                }}
                title={translate('cancel')}
                titleStyle={{color: appStyle.color11,}}
              />
            </View>
          </SafeAreaView>
        </Modal>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  clearLocalItems: () => {
    dispatch(clearItemsPendent())
  },
  confirmeItems: (table, items) => dispatch(confirmeItems(table, items)),
  sendOrder: () => dispatch(sendOrder()),
  // getTotalPendent: () => dispatch(getTotalPendent()),
  // getTotalSended: () => dispatch(getTotalSended()),
  // setTable: (table) => dispatch(setTable(table))
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderTableModal);
