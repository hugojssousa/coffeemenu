import React, {Component} from 'react';
import {View} from 'react-native';
import {Button} from 'react-native-elements';
import appStyle from '../style/styles.app';
import {translate} from '../locales';

export default class BtnCancel extends Component {

  constructor(props) {
    super(props)
    this.state = {
      modalBotaoAddPedidoVisible: true
    }
  }

  setModalVisible(visible) {
    this.setState({modalBotaoAddPedidoVisible: visible})
  }

  render() {
    return (
      <View>
        {
          this.props.showButton ?
            <Button
              onPress={() => {
                this.props.cancel()
              }}
              buttonStyle={{
                width: appStyle.width * 0.9,
                backgroundColor: appStyle.color2,
                borderRadius: 50,
                marginVertical: 5
              }}
              title={translate('cancel')}
              titleStyle={{color: appStyle.color11,}}
            />
            :
              null
        }
      </View>
    );
  }
}
