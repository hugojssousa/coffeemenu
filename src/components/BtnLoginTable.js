import React, {Component} from 'react';
import {View} from 'react-native';
import {Button} from 'react-native-elements';
import appStyle from '../style/styles.app';
import {translate} from '../locales';

export default class BtnLoginTable extends Component {

  render() {
    return (
      <View>
        {
          this.props.showButton ?
            <Button
              onPress={() => this.props.setModalVisible()}
              buttonStyle={{
                width: appStyle.width * 0.9,
                backgroundColor: appStyle.color2,
                borderRadius: 50,
              }}
              title={translate('textBtnLoginTable')}
              titleStyle={{color: appStyle.color11,}}
            />
            :
              null
        }
      </View>
    );
  }
}
