import React, {Component} from 'react';
import {View} from 'react-native';
import {Button} from 'react-native-elements';
import appStyle from '../style/styles.app';

export default class BotaoPedidos extends Component {

  constructor(props) {
    super(props)
    this.state = {
      modalBotaoAddPedidoVisible: true
    }
  }

  setModalVisible(visible) {
    this.setState({modalBotaoAddPedidoVisible: visible})
  }

  render() {
    return (
      <View>
        {
          this.props.showButton ?
            <Button
              onPress={() => this.props.action()}
              buttonStyle={{
                width: appStyle.width * 0.9,
                backgroundColor: appStyle.color2,
                borderRadius: 50,
              }}
              title={this.props.label}
              titleStyle={{color: appStyle.color11}}
            />
            :
              null
        }
      </View>
    )
  }
}
