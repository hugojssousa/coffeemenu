import React, {Component} from 'react';
import {ActivityIndicator, Text, TouchableOpacity, View} from 'react-native';
import {Image} from 'react-native-elements';

export default class CardItemCardapio extends Component {
  render() {
    return (
      <View style={[styles.cardMenu]}>
        <Image
          PlaceholderContent={<ActivityIndicator/>}
          source={this.props.item.img} style={{width: 100, height: 100, resizeMode: 'contain'}}/>

        <View style={[styles.viewItemLista]}>
          <Text style={styles.tituloItemLista}>{this.props.item.titulo}</Text>
          <Text style={styles.infoItemLista}>{this.props.item.ingredientes}</Text>
          <Text style={styles.precoItemLista}>R$ {this.props.item.preco}</Text>
        </View>

        <View style={styles.viewCart}>
          <TouchableOpacity
            style={styles.btnRemoveCart}
            onPress={() => {
              if (this.props.escolhidos > 0)
                this.setState({escolhidos: (this.state.escolhidos - 1)})
            }}>
            {
              this.state.escolhidos == 0 ?
                null
                :
                <Text style={styles.txtBtnRemoveCart}>-</Text>
            }
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.btnAddCart}
            onPress={() => this.setState({escolhidos: (this.state.escolhidos + 1)})}>
            {
              this.state.escolhidos > 0 ?
                <Text style={styles.txtBtnAddCart}>{this.state.escolhidos}</Text>
                :
                <Text style={styles.txtBtnAddCart}>+</Text>
            }
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
