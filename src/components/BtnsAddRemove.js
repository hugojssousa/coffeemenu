import React, {Component} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import appStyle from '../style/styles.app';


export default class BtnsAddRemove extends Component {

  constructor(props) {
    super(props)

    this.state = {
      qtdItens: props.qtdItens
    }
  }

  render() {
    return (
      <View style={[appStyle.styles.viewCart]}>
        <TouchableOpacity
          style={[appStyle.styles.btnRemoveCart]}
          onPress={() => {
            if (this.props.data.qtd > 0)
              this.props.remove(this.props.data)
          }}>
          {
            this.props.data.qtd === 0 ?
              null
              :
              <Text style={[appStyle.styles.txtBtnRemoveCart]}>-</Text>
          }
        </TouchableOpacity>

        <TouchableOpacity
          style={[appStyle.styles.btnAddCart]}
          onPress={() => this.props.add(this.props.data)}>
          {
            this.props.data.qtd > 0 ?
              <Text style={[appStyle.styles.txtBtnAddCart]}>{this.props.data.qtd}</Text>
              :
              <Text style={[appStyle.styles.txtBtnAddCart]}>+</Text>
          }
        </TouchableOpacity>
      </View>
    );
  }
}
