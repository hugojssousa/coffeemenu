/* @flow */

import React, {Component} from 'react';
import {ActivityIndicator, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {Button, Divider, Icon, Image, Input, ListItem} from 'react-native-elements';
import Modal from 'react-native-modal';
import ImagePicker from 'react-native-image-picker';
import {connect} from 'react-redux';
import ModalAddItem from '../components/ModalAddItem';
import appStyle from '../style/styles.app';

class ModalAddCategory extends Component {

  constructor(props) {
    super(props)
    this.state = {
      visibleModalItem: false,
      category: {
        id: '',
        type: '',
        name: '',
        img: '',
        items: [],
      }
    }
  }

  getIconBackHeader() {
    return (
      <Icon
        name='ios-close'
        type='ionicon' color={appStyle.color11}
        onPress={() => {this.props.setModalVisible()}}/>
    )
  }

  selecionarAvatar() {
    const options = {
      title: 'Select the image item',
      storageOptions: {
        skipBackup: true,
        path: 'images',
        cameraRoll: true,
        waitUntilSaved: true
      },
    };

    ImagePicker.launchImageLibrary(options, (response) => {
      const source = {uri: 'data:' + response.type + ';base64,' + response.data}

      if (source.uri !== '' || source.uri) {
        this.setState({
          category: {...this.state.category, img: source}
        })
      }
    })
  }

  setModalVisible() {
    this.setState({visibleModalItem: !this.state.visibleModalItem})
  }

  addItem(item) {
    item.id = this.state.category.items.length === 0 ? 0 : (this.state.category.items.length + 1)
    let category = this.state.category
    category.items.push(item)
    category.id = this.props.menu.categories.length + 1
    this.setState({category})
  }

  addCatgory() {
    let menu = this.props.menu
    menu.categories.push(this.state.category)
    this.props.setModalVisible()
  }

  render() {
    return (
        <Modal
          isVisible={this.props.visible}
          style={[appStyle.styles.modalAddItem]}>
          <View style={[appStyle.styles.viewModalAddItem]}>
            <View style={[appStyle.styles.titleModal]}>
              <Text style={[appStyle.styles.textTitleModal]}>Add Category</Text>
            </View>

            <View style={[appStyle.styles.modalBody]}>
              <TouchableOpacity
                style={{justifyContent: 'center', alignItems: 'center', marginBottom: 10}}
                onPress={() => {
                  this.selecionarAvatar()
                }}>
                {
                  !this.state.category.img || this.state.category.img === '' ?
                    <Icon
                      name="md-images"
                      type={'ionicon'}
                      size={100}
                      color={appStyle.color11}
                      iconStyle={{backgroundColor: appStyle.styles.color10}}
                    />
                    :
                    <Image
                      PlaceholderContent={<ActivityIndicator/>}
                      source={this.state.category.img} style={[appStyle.styles.imageEditItem]}/>
                }
                <Text>Tap here to choose new avatar</Text>
              </TouchableOpacity>

              <Input
                containerStyle={[appStyle.styles.marginVertical, appStyle.styles.inputAccount]}
                inputContainerStyle={[{borderBottomWidth: 0,}]}
                inputStyle={{paddingLeft: 10}}
                placeholder={'Type'}
                onChangeText={(text) => {this.setState({category: {...this.state.category, type: text}})}}
                value={this.state.category.type}
              />

              <Input
                containerStyle={[appStyle.styles.marginVertical, appStyle.styles.inputAccount]}
                inputContainerStyle={[{borderBottomWidth: 0,}]}
                inputStyle={{paddingLeft: 10}}
                placeholder={'Name'}
                onChangeText={(text) => {this.setState({category: {...this.state.category, name: text}})}}
                value={this.state.category.name}
              />

              <Text style={[appStyle.styles.subTitleModal]}>Items</Text>

              <Divider style={{backgroundColor: appStyle.color4, height: 0.5, width: appStyle.width * 0.95}} />

              {
                this.state.category.items.length > 0 ?
                  <ScrollView style={{height: appStyle.height * 0.2}}>
                    this.state.category.items.map(c => (
                        <ListItem
                          key={c.id}
                          leftAvatar={{source: c.img}}
                          onPress={() => {this.props.navigate(c.name)}}
                          title={c.name}
                          bottomDivider
                          chevron
                        />
                    ))
                  </ScrollView>
                  :
                  <Text>No items</Text>
              }

            </View>

            <Button
              buttonStyle={{
                width: appStyle.width * 0.8,
                backgroundColor: appStyle.color2,
                borderRadius: 50,
                marginVertical: 5
              }}
              title="Cancel"
              titleStyle={{color: appStyle.color11,}}
              onPress={() => this.props.setModalVisible()}
            />

            <Button
              buttonStyle={{
                width: appStyle.width * 0.8,
                backgroundColor: appStyle.color2,
                borderRadius: 50,
                marginVertical: 5
              }}
              disabled={this.state.category.type === '' || this.state.category.name === ''}
              title="Add item"
              titleStyle={{color: appStyle.color11,}}
              onPress={() => this.setModalVisible()}
            />

            <Button
              buttonStyle={{
                width: appStyle.width * 0.8,
                backgroundColor: appStyle.color2,
                borderRadius: 50,
                marginVertical: 5
              }}
              disabled={this.state.category.type === '' || this.state.category.name === ''}
              title="Add category"
              titleStyle={{color: appStyle.color11,}}
              onPress={() => this.addCatgory()}
            />
          </View>
          <ModalAddItem
            category={this.state.category.type}
            setModalVisible={() => this.setModalVisible()}
            visible={this.state.visibleModalItem}
            addItem={(item) => this.addItem(item)} />
        </Modal>
    )
  }
}

const mapStateToProps = (state) => (
  {
    menu: state.menu
  }
)

export default connect(mapStateToProps)(ModalAddCategory)
