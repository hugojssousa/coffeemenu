import React, {Component} from 'react';
import {ActivityIndicator, Alert, SafeAreaView, View, KeyboardAvoidingView, ScrollView} from 'react-native';
import {Button, Icon, Image, Input} from 'react-native-elements';
import appStyle from '../style/styles.app';
import {translate} from '../locales';
import {connect} from 'react-redux';
import {login} from '../redux/actions';
import util from '../util/Util';

class Login extends Component {

  constructor(props) {
    super(props);

    this.state = {
      localUser: {
        email: '',
        password: '',
      },
    };
  }

  loginValidation() {
    this.props.navigation.getParam('change');

    if (this.state.localUser.email === '' && this.state.localUser.password === '') {

      Alert.alert(
        'Validation',
        'Please enter your e-mail and password',
        [
          {text: 'Ok'},
        ],
        {cancelable: false},
      );

    } else if (this.state.localUser.email === '') {

      Alert.alert(
        'Validation',
        'Please enter your e-mail',
        [
          {
            text: 'Ok', onPress: () => {
            },
          },
        ],
        {cancelable: false},
      );

    } else if (!util.emailValidate(this.state.localUser.email)) {

      Alert.alert(
        'Validation',
        'Please enter a e-mail valide',
        [
          {
            text: 'Try again',
            onPress: () => {
              this.setState({
                localUser: {
                  name: '',
                  email: '',
                  password: '',
                  rePassword: '',
                },
              });
            },
          },
        ],
        {cancelable: false},
      );

    } else if (this.state.localUser.password === '') {

      Alert.alert(
        'Validation',
        'Please enter your password',
        [
          {
            text: 'Ok', onPress: () => {
            },
          },
        ],
        {cancelable: false},
      );

    } else {
      this.props.login(this.state.localUser);

      if (this.props.users.filter(u => u.logged)[0] && this.props.users.filter(u => u.logged)[0].logged) {

        this.setState({localUser: {email: '', password: ''}});
        this.props.navigation.navigate('HomeStack');

      } else {

        Alert.alert(
          'Validation',
          'Invalid credentials or account does not exist. Please check the information and try again.',
          [
            {
              text: 'Ok', onPress: () => {
              },
            },
          ],
          {cancelable: false},
        );

      }
    }
  }

  render() {

    return (
      <View style={[appStyle.styles.containerLogin]}>
        <SafeAreaView style={[appStyle.styles.viewLogin]}>
          <KeyboardAvoidingView style={[appStyle.styles.viewInputDynamic]} behavior="position" enabled>
            <View style={[appStyle.styles.viewAppLogo, {height: appStyle.height * 0.35}]}>
              <Image
                PlaceholderContent={<ActivityIndicator/>}
                source={require('img/logo-coffee.png')} style={{width: 240, height: 180}}/>
            </View>

            <Input
              containerStyle={[appStyle.styles.marginVertical, appStyle.styles.inputLogin]}
              inputContainerStyle={[{borderBottomWidth: 0}]}
              inputStyle={{paddingLeft: 10}}
              placeholder={translate('email')}
              onChangeText={(text) => {
                let localUser = this.state.localUser;
                localUser.email = text.toLowerCase();
                this.setState({localUser});
              }}
              value={this.state.localUser.email}
              leftIcon={
                <Icon
                  name="ios-mail"
                  type={'ionicon'}
                  size={24}
                  color={appStyle.color4}
                  iconStyle={{backgroundColor: appStyle.styles.color10}}
                />
              }
            />

            <Input
              containerStyle={[appStyle.styles.marginVertical, appStyle.styles.inputLogin]}
              inputContainerStyle={{borderBottomWidth: 0}}
              inputStyle={{paddingLeft: 10}}
              placeholder={translate('pass')}
              secureTextEntry={true}
              onChangeText={(text) => {
                let localUser = this.state.localUser;
                localUser.password = text;
                this.setState({localUser});
              }}
              value={this.state.localUser.password}
              leftIcon={
                <Icon
                  name="md-key"
                  type={'ionicon'}
                  size={24}
                  color={appStyle.color4}
                  iconStyle={{backgroundColor: appStyle.styles.color10}}
                />
              }
            />

            <Button
              buttonStyle={[appStyle.styles.btnLogin, appStyle.styles.marginBtnLogin]}
              titleStyle={[appStyle.styles.btnTitleLogin]}
              title={translate('enter')}
              onPress={
                () => {
                  this.loginValidation();
                }
              }
            />

            <Button
              buttonStyle={[appStyle.styles.btnLogin, appStyle.styles.marginBtnLogin]}
              titleStyle={[appStyle.styles.btnTitleLogin]}
              title={translate('signup')}
              onPress={
                () => {
                  this.setState({localUser: {email: '', password: ''}});
                  this.props.navigation.navigate('CreateAccountScreen');
                }
              }
            />

            {/*<Text style={[appStyle.styles.textoOuLogin]}>{translate('orAccess')}</Text>*/}

          </KeyboardAvoidingView>
        </SafeAreaView>
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  users: state.users,
});

const mapDispatchToProps = (dispatch) => ({
  login: (user) => {
    dispatch(login(user));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
