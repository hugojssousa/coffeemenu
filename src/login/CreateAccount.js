import React, {Component} from 'react';
import {ActivityIndicator, Alert, SafeAreaView, View} from 'react-native';
import {Container} from 'native-base';
import {Button, Icon, Image, Input} from 'react-native-elements';
import appStyle from '../style/styles.app';
import {translate} from '../locales';
import {connect} from 'react-redux';
import {addUser} from '../redux/actions';
import util from '../util/Util';

class CreateAccount extends Component {

  constructor(props) {
    super(props)

    this.state = {
      localUser: {
        name: '',
        email: '',
        password: '',
        rePassword: '',
      }
    }
  }

  accountValidation() {
    if (
      this.state.localUser.email.trim() === ''
      || this.state.localUser.password.trim() === ''
      || this.state.localUser.rePassword.trim() === ''
      || this.state.localUser.name.trim() === '') {

      Alert.alert(
        'Validation',
        'Please enter your name, e-mail and password',
        [
          {
            text: 'Try again'
          },
        ],
        {cancelable: false},
      )

    } else if (this.state.localUser.email === '') {

      Alert.alert(
        'Validation',
        'Please enter your e-mail',
        [
          {
            text: 'Try again', onPress: () => {
            }
          },
        ],
        {cancelable: false},
      )

    } else if (!util.emailValidate(this.state.localUser.email)) {

      Alert.alert(
        'Validation',
        'Please enter a e-mail valide',
        [
          {
            text: 'Try again', onPress: () => {
            }
          },
        ],
        {cancelable: false},
      )

    } else if (this.state.localUser.password === '') {

      Alert.alert(
        'Validation',
        'Please enter your password',
        [
          {
            text: 'Tray again', onPress: () => {
            }
          },
        ],
        {cancelable: false},
      )

    } else if (this.state.localUser.rePassword === '') {

      Alert.alert(
        'Validation',
        'Please repeat your password',
        [
          {
            text: 'Tray again', onPress: () => {
            }
          },
        ],
        {cancelable: false},
      )

    } else if (this.state.localUser.name === '') {

      Alert.alert(
        'Validation',
        'Please repeat your name',
        [
          {
            text: 'Tray again', onPress: () => {
            }
          },
        ],
        {cancelable: false},
      )

    } else if (this.state.localUser.rePassword !== this.state.localUser.password) {

      Alert.alert(
        'Validation',
        'Passwords must be the same',
        [
          {
            text: 'Tray again', onPress: () => {
            }
          },
        ],
        {cancelable: false},
      )

    } else {
      this.props.createAccount(this.state.localUser)

      Alert.alert(
        'Success :)',
        'account created!',
        [
          {
            text: 'Login',
            onPress: () => {
              this.setState({localUser: {email: '', password: '', rePassword: ''}})
              this.goToHome()
            }
          },
        ],
        {cancelable: false},
      )
    }
  }

  goToLogin() {
    this.props.navigation.navigate('LoginScreen', {change: true})
  }

  goToHome() {
    this.props.navigation.navigate('HomeScreen', {change: true})
  }

  goBack() {
    this.props.navigation.goBack()
  }

  render() {
    return (
      <Container style={[appStyle.styles.containerLogin]}>
        <SafeAreaView style={[appStyle.styles.viewLogin]}>
          <View style={[appStyle.styles.viewAppLogo, {height: appStyle.height * 0.35}]}>
            <Image
              PlaceholderContent={<ActivityIndicator/>}
              source={require('img/logo-coffee.png')} style={{width: 260, height: 200}}/>
          </View>

          <Input
            containerStyle={[appStyle.styles.marginVertical, appStyle.styles.inputLogin]}
            inputContainerStyle={[{borderBottomWidth: 0,}]}
            inputStyle={{paddingLeft: 10}}
            placeholder={translate('name')}
            onChangeText={(text) => {
              let localUser = this.state.localUser
              localUser.name = text
              this.setState({localUser})
            }}
            value={this.state.localUser.name}
            leftIcon={
              <Icon
                name="ios-person"
                type={'ionicon'}
                size={24}
                color={appStyle.color4}
                iconStyle={{backgroundColor: appStyle.styles.color10}}
              />
            }
          />

          <Input
            containerStyle={[appStyle.styles.marginVertical, appStyle.styles.inputLogin]}
            inputContainerStyle={[{borderBottomWidth: 0,}]}
            inputStyle={{paddingLeft: 10}}
            placeholder={translate('email')}
            onChangeText={(text) => {
              let localUser = this.state.localUser
              localUser.email = text.toLowerCase()
              this.setState({localUser})
            }}
            value={this.state.localUser.email}
            leftIcon={
              <Icon
                name="ios-mail"
                type={'ionicon'}
                size={24}
                color={appStyle.color4}
                iconStyle={{backgroundColor: appStyle.styles.color10}}
              />
            }
          />

          <Input
            containerStyle={[appStyle.styles.marginVertical, appStyle.styles.inputLogin]}
            inputContainerStyle={{borderBottomWidth: 0,}}
            inputStyle={{paddingLeft: 10}}
            placeholder={translate('pass')}
            secureTextEntry={true}
            onChangeText={(text) => {
              let localUser = this.state.localUser
              localUser.password = text
              this.setState({localUser})
            }}
            value={this.state.localUser.password}
            leftIcon={
              <Icon
                name="md-key"
                type={'ionicon'}
                size={24}
                color={appStyle.color4}
                iconStyle={{backgroundColor: appStyle.styles.color10}}
              />
            }
          />

          <Input
            containerStyle={[appStyle.styles.marginVertical, appStyle.styles.inputLogin]}
            inputContainerStyle={{borderBottomWidth: 0,}}
            inputStyle={{paddingLeft: 10}}
            placeholder={translate('rePass')}
            secureTextEntry={true}
            onChangeText={(text) => {
              let localUser = this.state.localUser
              localUser.rePassword = text
              this.setState({localUser})
            }}
            value={this.state.localUser.rePassword}
            leftIcon={
              <Icon
                name="md-key"
                type={'ionicon'}
                size={24}
                color={appStyle.color4}
                iconStyle={{backgroundColor: appStyle.styles.color10}}
              />
            }
          />

          <Button
            buttonStyle={[appStyle.styles.btnLogin, appStyle.styles.marginBtnLogin]}
            titleStyle={[appStyle.styles.btnTitleLogin]}
            title={translate('signup')}
            onPress={
              () => {
                this.accountValidation()
              }
            }
          />

          <Button
            buttonStyle={[appStyle.styles.btnLogin, appStyle.styles.marginBtnLogin]}
            titleStyle={[appStyle.styles.btnTitleLogin]}
            title={translate('cancel')}
            onPress={
              () => this.goToLogin()
            }
          />

        </SafeAreaView>
      </Container>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user
})

const mapDisptchToProps = (dispatch) => ({
  createAccount: (user) => dispatch(addUser(user))
})

export default connect(mapStateToProps, mapDisptchToProps)(CreateAccount)
