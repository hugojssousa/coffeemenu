import {Dimensions, Platform, StyleSheet} from 'react-native';

const appStyle = {}

appStyle.color1 = '#F2A636'
appStyle.color2 = '#FECE56'
appStyle.color3 = '#D99430'
appStyle.color4 = '#9A693B'
appStyle.color5 = '#D37653'
appStyle.color6 = '#ECECED'
appStyle.color7 = '#EACC9A'
appStyle.color8 = '#98C1D0'
appStyle.color9 = '#FE56CE'
appStyle.color10 = '#FFF'
appStyle.color11 = '#3e2723'
appStyle.color12 = '#cfd8dc'
appStyle.color1RGBA = '#F2A636'
appStyle.color2RGBA = '#FECE56'
appStyle.color3RGBA = '#D99430'
appStyle.color4RGBA = '#9A693B'
appStyle.color5RGBA = '#D37653'
appStyle.color6RGBA = 'rgba(236, 236, 237, 0.97)'
appStyle.color7RGBA = '#EACC9A'
appStyle.color8RGBA = '#98C1D0'
appStyle.color9RGBA = '#FE56CE'
appStyle.color10RGBA = '#FFF'
appStyle.color11RGBA = '#rgba(62, 39, 35, 0.97)'

appStyle.width = Dimensions.get('screen').width
appStyle.height = Dimensions.get('screen').height

appStyle.styles = StyleSheet.create({

  //APP
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: appStyle.color10,
  },
  containerSafeAreaView: {
    flex: 1,
    width: appStyle.width,
    height: appStyle.height
  },
  centerElements: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  rowElements: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  centerElementsFlex: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  widthDefault: {
    width: appStyle.width
  },
  heightDefault: {
    width: appStyle.height
  },
  widthBody: {
    width: appStyle.width * 0.9
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  point: {
    backgroundColor: appStyle.color4,
    width: 5,
    height: 5,
    borderRadius: 50,
    marginTop: 2,
    marginHorizontal: 10,
  },
  titleHeader: {
    color: appStyle.color11,
    fontWeight: '600',
    fontSize: 20,
  },
  viewBtnBusiness: {
    paddingVertical: 5,
    width: appStyle.width,
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: appStyle.color2,
    paddingHorizontal: 5,
  },
  logo: {
    width: 180, height: 140,
    resizeMode: 'stretch'
  },
  btnBusiness: {
    alignItems: 'center',
    justifyContent: 'center',
    height: appStyle.height * 0.03,
    borderRadius: 40,
    width: appStyle.width * 0.25,
    backgroundColor: appStyle.color10
  },
  btnBusinessClear: {
    alignItems: 'center',
    justifyContent: 'center',
    height: appStyle.height * 0.03,
    borderRadius: 40,
    // width: appStyle.width * 0.25,
    backgroundColor: appStyle.color10
  },
  textHeader: {
    color: appStyle.color11,
    marginBottom: 5,
  },
  viewAppLogo: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: appStyle.color2,
  },
  titleStyleBtnLocation: {
    color: appStyle.color4,
    fontSize: 12,
    height: appStyle.height * 0.02,

  },
  titleStyleBtnTable: {
    color: appStyle.color4,
    fontSize: 14,
    height: appStyle.height * 0.015,

  },
  titleStyleBtnFollow: {
    color: appStyle.color4,
    fontSize: 12,
    height: appStyle.height * 0.02,
  },
  textColorDefault: {
    color: appStyle.color11
  },
  bodyApp: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: appStyle.width * 0.9,
  },
  //END APP

  //LOGIN
  containerLogin: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: appStyle.color2,
  },
  viewLogin: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: appStyle.color2,
    width: appStyle.width * 0.9,
  },
  viewInputDynamic: {
    width: appStyle.width * 0.9,
    height: appStyle.height * 0.7,
    marginBottom: 10,
  },
  marginVertical: {
    marginVertical: 5
  },
  marginBottomImg: {
    marginBottom: 20
  },
  marginTopImg: {
    marginTop: 20
  },
  inputLogin: {
    borderRadius: 50,
    backgroundColor: appStyle.color10
  },
  inputAccount: {
    borderRadius: 50,
    borderColor: appStyle.color4,
    backgroundColor: appStyle.color6,
    borderWidth: 1,
  },
  inputModal: {
    borderRadius: 50,
    backgroundColor: appStyle.color7,
    marginVertical: 10,
  },
  imputMoney: {
    width: appStyle.width * 0.9,
    height: 40,
    maxHeight: 40,
    paddingLeft: 20,
    fontSize: 18
  },
  viewLogo: {
    width: appStyle.width * 0.9,
    height: appStyle.height * 0.4
  },
  imgLogo: {
    width: 100,
    height: 200,
    resizeMode: 'center',
  },
  btnLogin: {
    borderRadius: 50,
    backgroundColor: appStyle.color11,
    width: appStyle.width * 0.9,
  },
  btnSave: {
    borderRadius: 50,
    backgroundColor: appStyle.color2,
    width: appStyle.width * 0.9,
  },
  btnLoginSocial: {
    borderRadius: 50,
    width: appStyle.width * 0.9,
    height: appStyle.height * 0.05,
    paddingTop: 10
  },
  marginBtnLogin: {
    marginTop: 10
  },
  btnTitleLogin: {
    color: appStyle.color2
  },
  btnTitleSave: {
    color: appStyle.colo11
  },
  textoOuLogin: {
    color: appStyle.color10,
    marginVertical: 10,
    fontSize: 16
  },

  cards: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-around',
    flexWrap: 'wrap',
  },
  card: {
    justifyContent: 'center',
    alignItems: 'center',
    width: appStyle.width * 0.4,
    height: appStyle.height * 0.2,
  },
  cardTable: {
    marginVertical: 10,
    marginHorizontal: 10,
    paddingVertical: 5,
    paddingHorizontal: 5,
    backgroundColor: appStyle.color11,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    width: appStyle.width * 0.15,
    height: appStyle.width * 0.15,
  },
  cardAddTable: {
    marginVertical: 10,
    marginHorizontal: 10,
    paddingVertical: 5,
    paddingHorizontal: 5,
    backgroundColor: appStyle.color2,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    width: appStyle.width * 0.15,
    height: appStyle.width * 0.15,
  },
  textCardTable: {
    fontSize: 32,
    color: appStyle.color2
  },
  textoCard: {
    color: appStyle.color4,
    fontSize: 18,
    fontWeight: '600'
  },
  marginTop: {
    marginTop: 10
  },
  cardMenu: {
    flex: 1,
    marginVertical: 10,
    paddingLeft: 10,
    flexDirection: 'row',
    width: appStyle.width,
    height: appStyle.height * 0.12,
  },
  viewCart: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    width: appStyle.width * 0.3,
    height: appStyle.height * 0.1
  },
  btnRemoveCart: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 42,
    height: 42,
    borderRadius: 50,
  },
  txtBtnRemoveCart: {
    textAlignVertical: 'center',
    color: appStyle.color11,
    height: 42,
    fontSize: 38,
    fontWeight: '600',
  },
  btnAddCart: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 42,
    height: 42,
    borderRadius: 50,
    backgroundColor: appStyle.color2,
  },
  txtBtnAddCart: {
    textAlignVertical: 'center',
    height: 42,
    fontSize: 28,
    fontWeight: '600',
    paddingTop: 3,
    color: appStyle.color11,
  },
  viewItemLista: {
    paddingLeft: 10,
    paddingTop: 5,
    width: appStyle.width * 0.52,
    height: appStyle.height * 0.1,
    flexDirection: 'column',
  },
  tituloItemLista: {
    fontSize: 14,
    color: appStyle.color11,
    fontWeight: '600',
  },
  infoItemLista: {
    color: appStyle.color9,
    fontWeight: '500',
    fontSize: 12,
  },
  precoItemLista: {
    color: appStyle.color11,
    fontSize: 16,
    fontWeight: '500',
  },

  cardPedido: {
    borderRadius: 20,
    borderWidth: 1,
    borderColor: appStyle.color4,
    marginVertical: 10,
    paddingBottom: 10
  },
  viewTitlePedidos: {
    flexDirection: 'row',
    paddingVertical: 20
  },
  titlePedidos1: {
    fontSize: 20,
    fontWeight: '800',
  },
  titlePedidos2: {
    fontSize: 20,
  },
  viewCardItemPedido: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  cardItemPedido: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 5,
  },
  cardImg: {
    width: 40,
    height: 40,
    resizeMode: 'contain'
  },
  cardItemPedidoTitulo: {
    flexWrap: 'wrap',
    width: appStyle.width * 0.6,
    paddingLeft: 5,
  },
  cardItemNome: {
  },
  cardItemDescricao: {
    color: appStyle.color9,
    fontSize: 10,
  },
  cardItemValor: {
    color: appStyle.color4,
    fontSize: 14,
  },
  cardTotalPedido: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 3,
    marginTop: appStyle.height * 0.05,
    marginBottom: appStyle.height * 0.02,
    borderBottomColor: appStyle.color11,
    borderBottomWidth: 0.3,
  },
  cardDividirPedido: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 3,
    marginBottom: appStyle.height * 0.02,
    borderBottomColor: appStyle.color11,
    borderBottomWidth: 0.3,
  },
  cardTotal: {
    fontSize: 22,
    color: appStyle.color11
  },
  cardTotalValor: {
    fontSize: 22,
    color: appStyle.color11
  },
  textNoOrder: {
    marginVertical: 20,
    fontSize: 22
  },
  marginBtn: {
    marginVertical: 5
  },
  tituloModal: {
    fontSize: 26,
    fontWeight: '500',
    color: appStyle.color11,
  },
  viewModal: {
    backgroundColor: appStyle.color6RGBA,
    width: appStyle.width,
    height: appStyle.height
  },
  numberTable: {
    fontSize: 12,
    color: appStyle.color6,
  },
  viewNumberTable: {
    borderRadius: 50,
    backgroundColor: appStyle.color11,
    width: appStyle.width * 0.08,
    height: appStyle.width * 0.08,
    marginLeft: 10,
    marginTop: -5
  },
  viewTitleTable: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: appStyle.color11,
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderRadius: 50,
    width: appStyle.width * 0.2,
    height: appStyle.width * 0.2,
    marginVertical: 10,
  },
  viewNumberTableModal: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
    backgroundColor: appStyle.color11,
    width: appStyle.width * 0.1,
    height: appStyle.width * 0.1,
  },
  titleTableModal: {
    fontSize: 42,
    fontWeight: '500',
    color: appStyle.color11,
  },
  numberTableModal: {
    fontSize: 16,
    color: appStyle.color6,
  },
  titleTable: {
    fontSize: 42,
    color: appStyle.color2,
  },
  textOrderModal: {
    color: appStyle.color11,
    fontSize: 20
  },
  textTableModal: {
    fontSize: 16
  },
  tableModal: {
    height: appStyle.width * 0.15,
    width: appStyle.width * 0.15,
    paddingHorizontal: 5,
    paddingVertical: 5,
    marginHorizontal: 5,
    marginVertical: 10,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
  },
  titleWithTableModal: {
    alignItems: 'center',
    flexDirection: 'row'
  },
  btnCard: {
    width: appStyle.width * 0.8,
    backgroundColor: appStyle.color2,
    borderRadius: 50,
    marginVertical: 5,
  },
  borderImageItem: {
    borderRadius: 50,
    width: 150,
    height: 150,
  },
  imageEditItem: {
    width: 80,
    height: 80,
    resizeMode: 'contain',
  },
  imageEditItemMini: {
    width: 40,
    height: 40,
    borderRadius: 15,
    resizeMode: 'contain',
    overflow: 'hidden',
  },
  iconEditImageItem: {
    marginTop: -120,
    marginLeft: 90,
    paddingTop: -10,
  },
  viewLogoAdmin: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoItem: {
    margin: 20,
    height: 10,
    width: 10
  },
  safeAreaView: {
    flex: 1,
    width: appStyle.width,
    height: appStyle.height,
    alignItems: 'center'
  },
  titleItems: {
    width: appStyle.width * 0.9,
    textAlign: 'left',
    fontSize: 20,
  },
  viewModalAddItem: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    borderRadius: 5,
    padding: 10,
    backgroundColor: appStyle.color6
  },
  modalAddItem: {

  },
  modalBody: {
    width: appStyle.width * 0.8,
    marginVertical: 10,
  },
  modalItemBody: {
    width: appStyle.width * 0.8,
    height: appStyle.height * 0.5,
    marginVertical: 10,
  },
  titleModal: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
  },
  textTitleModal: {
    fontSize: 20,
    fontWeight: '700',
  },
  subTitleModal: {
    fontSize: 17,
    fontWeight: '600',
    marginVertical: 15,
  },
  viewSwitch: {
    width: appStyle.width * 0.9,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    marginVertical: 0,
  },
  labelSwitch: {
    fontSize: 17
  },
  titleCategory: {
    fontSize: 20,
    marginLeft: 15,
    marginTop: 25,
    marginBottom: 10,
  },
  dividerSmall: {
    backgroundColor: appStyle.color4,
    height: 0.5,
    width: appStyle.width * 0.9
  },
  dividerLarge: {
    backgroundColor: appStyle.color4,
    height: 0.5,
  },
  titleEditCategory: {
    color: appStyle.color11,
    fontWeight: '600',
    fontSize: 26
  },
  viewAvatarEditCategory: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
    height: appStyle.height * 0.18,
  },
  textStatusItem: {
    color: 'red',
    fontSize: 18,
    fontWeight: '600',
    borderWidth: 0.5,
    borderColor: 'gray',
    paddingHorizontal: 10,
    paddingVertical: 2,
    borderRadius: 13
  },
  iconBack: {
    width: 30,
  },
  imageItem: {
    width: 70,
    height: 80,
    resizeMode: 'contain'
  },
  imageItemView: {
    width: 50,
    height: 80,
    justifyContent: 'center'
  },
  heightHeader: {
    borderBottomWidth: 0,
    height: Platform.OS === 'android' ? 50 : 80,
    paddingTop: Platform.OS === 'android' ? -20 : 40
  }
})

export default appStyle
