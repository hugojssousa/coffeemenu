import React, {Component} from 'react';
import {Provider} from 'react-redux';
import store from './redux/store';
import Navigation from './Navigation';
import Icon from 'react-native-vector-icons/Ionicons'

Icon.loadFont()

export default class App extends Component<Props> {

  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Provider store={store}>
        <Navigation />
      </Provider>
    );
  }
}
