import React, {Component} from 'react';
import {
  ActivityIndicator, Alert, SafeAreaView, ScrollView, Text, TouchableOpacity,
  View, Platform} from 'react-native';
import {connect} from 'react-redux';
import appStyle from './style/styles.app';
import {Header, Icon, Image} from 'react-native-elements';
import {translate} from './locales';
import {Container, Tab, TabHeading, Tabs} from 'native-base';
import TabMenu from './components/TabMenu';
import TabTables from './components/TabTables';
import {clearItemsPending, setCategory, setSelectedTable, settings} from './redux/actions';
import CancelTableModal from './components/CancelTableModal';
import CardPendingOrder from './components/CardPendingOrder';
import CardConsumption from './components/CardConsumption';

class Home extends Component {

  constructor(props) {
    super(props)

    this.state = {
      modalCancelVisible: false
    }
  }

  goToCategory(category) {
    this.props.setCategory(category)
    this.props.navigation.navigate('CategoryScreen')
  }

  goToTable() {
    this.props.navigation.navigate('TableScreen')
  }

  setModalCancelVisible() {
    this.setState({modalCancelVisible: !this.state.modalCancelVisible})
  }

  finishOrder() {

    let message = ''
    let totalPriceItems = 0

    this.props.table.items.map((o) => {
      if (o.status === 'P') {
        o.items.map(i => {
          message += i.totalItemsPending + ' ' + i.name + '\n'
        })
      }
    })

    this.props.table.items.map((o) => {
      if (o.status === 'P') {
        o.items.map(i => {
          if (i.totalItemsPending > 0) {
            totalPriceItems += i.price * i.totalItemsPending
          }
        })
      }
    })

    message += translate('money') + ' ' + totalPriceItems.toFixed(2)

    Alert.alert(
      translate('textAlertConfirmOrder'),
      message,
      [
        {
          text: translate('textAlertCancelItem'),
          onPress: () => {
          },
          style: 'cancel',
        },
        {
          text: translate('textAlertOkItem'),
          onPress: () => {
            this.props.table.items.map((o) => {
              if (o.status === 'P') {
                o.status = 'S'
              }
            })

            this.props.categories.map(c => {
              c.items.map(i => {
                i.totalItemsSended = i.totalItemsPending
                i.totalItemsPending = 0
              })
            })
            this.forceUpdate()
          },
        },
      ],
      {cancelable: false},
    )
  }

  closeConsumption() {
    Alert.alert(
      translate('closeAccount'),
      'We are grateful for your preference.\n',
      [
        {
          text: translate('txtBtnOk'),
          onPress: () => {
            this.props.categories.map(c => {
              c.items.map(i => {
                i.totalItemsSended = 0
              })
            })
            this.props.table.items = []
            this.forceUpdate()
          },
          style: 'ok',
        },
      ],
      {cancelable: false},
    )
  }

  cancelPending() {
    Alert.alert(
      translate('cancelOrder'),
      'Are you sure you want to cancel?\n',
      [
        {
          text: translate('cancel'),
          onPress: () => {
          },
          style: 'cancel',
        },
        {
          text: translate('txtBtnOk'),
          onPress: () => {
            this.props.clearItemsPending()
            this.forceUpdate()
          },
          style: 'ok',
        },
      ],
      {cancelable: false},
    )
  }

  render() {
    return (
      <Container style={appStyle.styles.container}>
        <Header
          backgroundColor={appStyle.color2}
          centerComponent={{text: 'Hometown Coffee', style: [appStyle.styles.titleHeader]}}
          centerContainerStyle={{width: appStyle.width}}
          containerStyle={[appStyle.styles.heightHeader]}
          rightComponent={
            this.props.user && this.props.user.userAdmin ?
              this.props.user.img !== '' ?

                <View style={[appStyle.styles.rowElements]}>
                  <TouchableOpacity
                    style={{marginRight: 10, borderRadius: 15}}
                    onPress={() => {
                      this.props.navigation.navigate('AccountStack')
                    }
                    }>
                    <Image
                      style={{paddingTop: -5}}
                      PlaceholderContent={<ActivityIndicator/>}
                      source={this.props.user.img} style={[appStyle.styles.imageEditItemMini]}/>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={() => {
                    this.props.setSettings(true)
                    this.props.navigation.navigate('SettingsStack')
                  }}>
                    <Icon
                      name="ios-settings"
                      type="ionicon"
                      size={34}
                      color={appStyle.color11}
                    />
                  </TouchableOpacity>
                </View>

                :

                <View style={[appStyle.styles.rowElements]}>
                  <TouchableOpacity
                    style={{marginRight: 10}}
                    onPress={() => {
                      this.props.navigation.navigate('AccountStack')
                    }}>
                    <Icon
                      name="ios-person"
                      type="ionicon"
                      size={34}
                      color={appStyle.color11}
                    />
                  </TouchableOpacity>

                  <TouchableOpacity onPress={() => {
                    this.props.setSettings(true)
                    this.props.navigation.navigate('SettingsStack')
                  }}>
                    <Icon
                      name="ios-settings"
                      type="ionicon"
                      size={38}
                      color={appStyle.color11}
                    />
                  </TouchableOpacity>
                </View>

              :

              this.props.table ?
                <View style={[appStyle.styles.rowElements]}>

                  {
                    this.props.user && this.props.user.img ?
                      <TouchableOpacity onPress={() => {
                        this.props.navigation.navigate('AccountStack')
                      }}>
                        <Image
                          PlaceholderContent={<ActivityIndicator/>}
                          source={this.props.user.img} style={[appStyle.styles.imageEditItemMini]}/>
                      </TouchableOpacity>

                      :

                      <TouchableOpacity onPress={() => {
                        this.props.navigation.navigate('AccountStack')
                      }}>
                        <Icon
                          name="ios-person"
                          type="ionicon"
                          size={38}
                          color={appStyle.color11}
                        />
                      </TouchableOpacity>
                  }

                  <TouchableOpacity
                    style={[appStyle.styles.viewNumberTable, appStyle.styles.centerElements]}
                    onPress={() => this.setModalCancelVisible()}
                  >
                    <Text style={[appStyle.styles.numberTable]}>{this.props.table.number}</Text>
                  </TouchableOpacity>
                </View>

                :

                  this.props.user && this.props.user.img ?
                    <View style={[appStyle.styles.rowElements]}>
                      <TouchableOpacity onPress={() => {
                        this.props.navigation.navigate('AccountStack')
                      }}>
                        <Image
                          PlaceholderContent={<ActivityIndicator/>}
                          source={this.props.user.img} style={[appStyle.styles.imageEditItemMini]}/>
                      </TouchableOpacity>
                    </View>
                  :
                    <View style={[appStyle.styles.rowElements]}>
                      <TouchableOpacity onPress={() => {
                        this.props.navigation.navigate('AccountStack')
                      }}>
                        <Icon
                          name="ios-person"
                          type="ionicon"
                          size={38}
                          color={appStyle.color11}
                        />
                      </TouchableOpacity>
                    </View>

          }
        />

        <SafeAreaView style={{flex: 1, width: appStyle.width, height: appStyle.height}}>

          <View>
            <View style={[appStyle.styles.viewAppLogo]}>
              <Image
                PlaceholderContent={<ActivityIndicator/>}
                source={require('./img/logo-coffee.png')} style={[appStyle.styles.logo]}/>
              <Text style={[appStyle.styles.textHeader]}>{translate('slogan')}</Text>

            </View>
          </View>

          <Tabs tabBarUnderlineStyle={{backgroundColor: appStyle.color4,}}>
            <Tab
              style={{backgroundColor: appStyle.color10}}
              heading={
                <TabHeading
                  activeTextStyle={{color: appStyle.color10}}
                  style={{backgroundColor: appStyle.color10,}}>
                  <Text style={{color: appStyle.color11, fontSize: 18}}>{translate('menu')}</Text>
                </TabHeading>
              }>
              <TabMenu
                goToItem = {(category) => this.goToCategory(category)}
              />
            </Tab>

            {
              this.props.user && this.props.user.userAdmin ?
                <Tab
                  style={{backgroundColor: appStyle.color10}}
                  heading={
                    <TabHeading style={{backgroundColor: appStyle.color10,}}>
                      <Text style={{color: appStyle.color11, fontSize: 18}}>{translate('tables')}</Text>
                    </TabHeading>
                  }>
                  <TabTables
                    setTable={(table) => this.props.setSelectedTable(table)}
                    goToTable={() => this.goToTable()}/>
                </Tab>

                :

                <Tab
                  style={{backgroundColor: appStyle.color10}}
                  heading={
                    <TabHeading style={{backgroundColor: appStyle.color10,}}>
                      <Text style={{color: appStyle.color11, fontSize: 18}}>{translate('orders')}</Text>
                    </TabHeading>
                  }>

                  <ScrollView>
                    <View style={[appStyle.styles.centerElements]}>
                      {
                        this.props.table && this.props.table.status ===  'O' && this.props.table.items.length === 0 ?
                          <Text style={[appStyle.textColorDefault, appStyle.styles.textNoOrder]}>{translate('noOrder')}</Text>
                          :
                          null
                      }

                      {
                        !this.props.table ?
                          <Text style={[appStyle.textColorDefault, appStyle.styles.textNoOrder]}>{translate('noTableOpened')}</Text>
                          :
                          null
                      }

                      {
                        this.props.table && this.props.table.status === 'O' && this.props.table.items.length > 0 ?
                          (
                            this.props.table.items.map((o) => {
                              if (o.status === 'P' && o.items.length > 0) {
                                return (
                                  <CardPendingOrder
                                    key={o.items.length}
                                    items={o.items}
                                    finishOrder={() => this.finishOrder()}
                                    cancel={() => this.cancelPending()}
                                  />
                                )
                              }
                            })
                          )
                          :
                          null
                      }

                      {
                        this.props.table && this.props.table.status === 'O' && this.props.table.items.length > 0 ?
                          (
                            this.props.table.items.map((o) => {
                              if (o.status === 'S') {
                                return (
                                  <CardConsumption
                                    key={o.items.length}
                                    items={o.items}
                                    closeConsumption={() => this.closeConsumption()}
                                  />
                                )
                              }
                            })
                          )
                          :
                          null
                      }
                    </View>
                  </ScrollView>

                </Tab>
            }
          </Tabs>

          <CancelTableModal
            closeModal={() => this.setModalCancelVisible()}
            modalVisible={() => this.state.modalCancelVisible} />

        </SafeAreaView>
      </Container>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.users.filter((u) => u.logged)[0],
  users: state.users,
  tables: state.tables,
  categories: state.menu.categories,
  table: state.tables.filter(t => t.status === 'O')[0],
})

const mapDispatchToProps = (dispatch) => ({
  setSettings: (payload) => dispatch(settings(payload)),
  clearItemsPending: () => dispatch(clearItemsPending()),
  setSelectedTable: (table) => dispatch(setSelectedTable(table)),
  setCategory: (payload) => dispatch(setCategory(payload)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Home)
