import {translate} from '../../locales';
import {Alert} from 'react-native';

export default (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_USER':
      let newUser = {
        id: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15),
        name: action.payload.name,
        email: action.payload.email.toLocaleLowerCase(),
        password: action.payload.password,
        img: '',
        myOrders: [],
        account: [],
        userAdmin: false,
        logged: true,
        settings: false,
        table: {
          number: 0,
          usersLoggedOnTheTable: [],
        },
      }

      let usersAdd = state.users
      usersAdd.push(newUser)

      return Object.assign({}, state, {
        users: usersAdd
      })

    case 'EDIT_USER':

      let usersEdited = state.users.map(u => {
        if (u.id === action.payload.id) {
          u = action.payload
        }
        return u
      })

      return Object.assign({}, state, {
        users: usersEdited
      })

    case 'LOGIN':
      let userLogin = action.payload
      let usersLogin = []

      if (userLogin) {
        usersLogin = state.users.map(u => {
          if (u.email === userLogin.email && u.email.includes('admin')) {
            u.logged = true
            u.settings = true
            u.userAdmin = true
          } else if (u.email === userLogin.email) {
            u.logged = true
          }

          return u
        })
      }

      return Object.assign({}, state, {
        users: usersLogin
      })

    case 'LOGOUT':
      let usersLogout = state.users
      let usersLogoutUpdated = []

      usersLogoutUpdated = usersLogout.map(u => {
        if (u.email.includes('admin')) {
          u.logged = false
          u.settings = false
          u.userAdmin = false
        } else {
          u.logged = false
        }

        return u
      })

      return Object.assign({}, state, {users: usersLogoutUpdated})

    case 'SETTINGS':
      let usersSettings = state.users.map(u => {
        if (action.payload.id === u.id) {
          u.settings = action.payload.setting
        }
        return u
      })

      return Object.assign({}, state, {
        users: usersSettings
      })

    case 'OPEN_TABLE':
      let userTable = state.user

      let tablesOpened = state.tables.map(t => {
        if (action.payload === t.number) {
          t.users.push(userTable)
          t.status = 'O'
        }

        return t
      })

      return Object.assign({}, state, {
        tables: tablesOpened
      })

    case 'CANCEL_TABLE':
      let tablesClear = state.tables.map(t => {
        if (t.status === 'O') {
          t.status = ''
          t.users = []
        }

        return t
      })

      return Object.assign({}, state, {
        tables: tablesClear
      })

    case 'CLEAR_ITEMS_PENDING':

      let clearTablesPending = state.tables.map(t => {
        if (t.status === 'O') {
          t.items = t.items.filter(o => o.status === 'S')
        }
        return t
      })

      let clearCaterogiesPending = state.menu.categories.map(c => {
        c.items.map(i => {
          i.totalItemsPending = 0
        })
        return c
      })

      let clearMenuPending = state.menu
      clearMenuPending.categories = clearCaterogiesPending

      return Object.assign({}, state, {
        tables: clearTablesPending,
        menu: clearMenuPending
      })

    case 'ADD_TABLE':
      let tables = state.tables
      let newTable = {number: tables.length + 1, items: []}
      tables.push(newTable)

      return Object.assign({}, state, {tables: tables})

    case 'REMOVE_TABLE':
      let tablesDel = state.tables
      let lastTable = tablesDel.filter((t) => t.number === tablesDel.length)[0]
      if (lastTable.items && lastTable.items.length === 0) {
        tablesDel.pop()
      } else {
        Alert.alert(
          'Validation',
          `The table ${lastTable.number} can not be removed because it is being used.`,
          [
            {
              text: 'Ok', onPress: () => {
              }
            },
          ],
          {cancelable: false},
        )
      }

      return Object.assign({}, state, {tables: tablesDel})

    case 'SET_SELECTED_TABLE':
      return Object.assign({}, state, {
        selectedTable: action.payload
      })

    case 'SET_TOTAL_SELECTED_TABLE':
      let totalPriceSeletdTable = 0.0

      state.tables.map(t => {
        if (state.selectedTable.number === t.number) {
          t.items.map(i => {
            if (i.totalSended > 0) {
              totalPriceSeletdTable += (i.price * i.totalSended)
            }
          })
        }
      })

      return Object.assign({}, state, {
        priceTableConfirmed: totalPriceSeletdTable
      })

    case 'SET_SELECTED_ITEM_MENU':
      return Object.assign({}, state, {idItemSelected: action.payload})

    case 'DEL_SELECTED_ITEM_MENU':
      return Object.assign({}, state, {idItemSelected: 0})

    case 'EDIT_CATEGORY':
      let menuEdit = state.menu
      let categoriesEdited = menuEdit.categories.map((i) => {
        if (i.id === action.payload.id) {
          i = action.payload
        }
        return i
      })

      menuEdit.categories = categoriesEdited

      return Object.assign({}, state, {menu: menuEdit})

    case 'SET_CATEGORY':
      if (action.payload) {
        return Object.assign({}, state, {categorySelected: action.payload})
      } else {
        return Object.assign({}, state, {
          categorySelected: {
            id: 0,
            type: '',
            name: '',
            img: '',
            active: true,
            items: [],
          }
        })
      }

    case 'ADD_CATEGORY':
      let menuAdd = state.menu
      let categorySended = action.payload
      categorySended.id = state.menu.categories.length + 1
      menuAdd.categories.push(categorySended)

      return Object.assign({}, state, {menu: menuAdd})

    case 'DELETE_CATEGORY':
      let menuDelete = state.menu

      let menuDeleted = menuDelete.categories.map(c => {
        if (c.id === action.payload.id) {
          c.active = false
          return c
        }
      })

      return Object.assign({}, state, {menu: menuDeleted})

    case 'DELETE_ITEM':
      let menuDeleteItem = state.menu

      let menuDeletedItem = menuDelete.categories.map(c => {
        c.items.map(i => {
          if (i.id === action.payload.id) {
            i.active = false
            return i
          }
          return c
        })
      })

      return Object.assign({}, state, {menu: menuDeletedItem})

    case 'SET_SELECTED_ITEM':
      if (action.payload) {
        return Object.assign({}, state, {itemSelected: action.payload})
      } else {
        return Object.assign({}, state, {
          itemSelected: {
            id: 0,
            type: '',
            name: '',
            size: '',
            description:'',
            price: 0.0,
            img: '',
            totalItemsPending: 0,
            totalItemsSended: 0,
            totalItemsAdminPending: 0,
            totalItemsAdminSended: 0,
            active: true
          }
        })
      }

    case 'ADD_ITEM':
      let categorySelectedAdd = state.categorySelected
      let itemSended = action.payload
      itemSended.id = categorySelectedAdd.items.length + 1
      categorySelectedAdd.items.push(itemSended)

      return Object.assign({}, state, {categorySelected: categorySelectedAdd})

    case 'EDIT_ITEM':
      let categorySelectedEdit = state.categorySelected
      let itemsEdited = categorySelectedEdit.items.map(i => {
        if (i.id === action.payload.id) {
          i = action.payload
        }
        return i
      })

      categorySelectedEdit.items = itemsEdited

      return Object.assign({}, state, {categorySelected: categorySelectedEdit})

    default:
      return state
  }

}

const initialState = {
  users: [
    {
      id: 'j4j5sfculmc9f5tgn23859',
      name: 'Hugo Sousa',
      email: 'admin@hotmail.com',
      password: '123',
      img: '',
      account: [],
      userAdmin: false,
      logged: false,
      settings: false,
    },
    {
      id: 'j4j5sfculmc9f5tgn23412',
      name: 'Hugo Sousa Admin',
      email: 'goj1@hotmail.com',
      password: '123',
      img: '',
      account: [],
      userAdmin: false,
      logged: false,
      settings: false,
    }
  ],
  myPendentOrder: {},
  allOrders: [],
  selectedTable: {},
  idItemSelected: 0,
  categorySelected: {},
  itemSelected: {},
  tempItems: [],
  menu: {
    categories: [
      {
        id: 1,
        type: 'coffee',
        name: 'Coffees',
        img: require('img/icon-coffee.png'),
        active: true,
        items: [
          {
            id: 1,
            type: 'coffee',
            name: translate('coffeePingado'),
            size: '150ml',
            description: translate('coffeePingadoDesc'),
            price: 8.90,
            img: require('img/icon-coffee-1.png'),
            totalItemsPending: 0,
            totalItemsSended: 0,
            totalItemsAdminPending: 0,
            totalItemsAdminSended: 0,
            active: true
          },
          {
            id: 2,
            type: 'coffee',
            name: translate('coffeeLateChantilly'),
            size: '300ml',
            description: translate('coffeeLateChantillyDesc'),
            price: 16.90,
            img: require('img/icon-coffee-2.png'),
            totalItemsPending: 0,
            totalItemsSended: 0,
            totalItemsAdminPending: 0,
            totalItemsAdminSended: 0,
            active: true
          },
          {
            id: 3,
            type: 'coffee',
            name: translate('coffeeGold'),
            size: '350ml',
            description: translate('coffeeGoldDesc'),
            price: 21.90,
            img: require('img/icon-coffee-3.png'),
            totalItemsPending: 0,
            totalItemsSended: 0,
            totalItemsAdminPending: 0,
            totalItemsAdminSended: 0,
            active: true
          },
          {
            id: 4,
            type: 'coffee',
            name: translate('coffeeSupremo'),
            size: '250ml',
            description: translate('coffeeSupremoDesc'),
            price: 25.90,
            img: require('img/icon-coffee-4.png'),
            totalItemsPending: 0,
            totalItemsSended: 0,
            totalItemsAdminPending: 0,
            totalItemsAdminSended: 0,
            active: true
          },
          {
            id: 5,
            type: 'coffee',
            name: translate('coffeeSimples'),
            size: '100ml',
            description: translate('coffeeSimplesDesc'),
            price: 7.90,
            img: require('img/icon-coffee-5.png'),
            totalItemsPending: 0,
            totalItemsSended: 0,
            totalItemsAdminPending: 0,
            totalItemsAdminSended: 0,
            active: true
          }
        ]
      },
      {
        id: 2,
        type: 'cupcake',
        name: 'Cupcakes',
        img: require('img/icon-cacke.png'),
        active: true,
        items: [
          {
            id: 7,
            type: 'cupcake',
            name: translate('cherryCake'),
            size: '150g',
            description: translate('cherryCakeDesc'),
            price: 8.90,
            img: require('img/icon-cacke-1.png'),
            totalItemsPending: 0,
            totalItemsSended: 0,
            totalItemsAdminPending: 0,
            totalItemsAdminSended: 0,
            active: true
          },
          {
            id: 8,
            type: 'cupcake',
            name: translate('strawberryCake'),
            size: '250g',
            description: translate('strawberryCakeDesc'),
            price: 6.90,
            img: require('img/icon-cacke-2.png'),
            totalItemsPending: 0,
            totalItemsSended: 0,
            totalItemsAdminPending: 0,
            totalItemsAdminSended: 0,
            active: true
          },
          {
            id: 9,
            type: 'cupcake',
            name: translate('cakeKids'),
            size: '100g',
            description: translate('cakeKidsDesc'),
            price: 5.90,
            img: require('img/icon-cacke-3.png'),
            totalItemsPending: 0,
            totalItemsSended: 0,
            totalItemsAdminPending: 0,
            totalItemsAdminSended: 0,
            active: true
          },
          {
            id: 10,
            type: 'cupcake',
            name: translate('superCake'),
            size: '350g',
            description: translate('superCakeDesc'),
            price: 11.90,
            img: require('img/icon-cacke-4.png'),
            totalItemsPending: 0,
            totalItemsSended: 0,
            totalItemsAdminPending: 0,
            totalItemsAdminSended: 0,
            active: true
          },
          {
            id: 11,
            type: 'cupcake',
            name: translate('cakeCuddly'),
            size: '200g',
            description: translate('cakeCuddlyDesc'),
            price: 7.90,
            img: require('img/icon-cacke-5.png'),
            totalItemsPending: 0,
            totalItemsSended: 0,
            totalItemsAdminPending: 0,
            totalItemsAdminSended: 0,
            active: true
          },
        ]
      },
      {
        id: 3,
        type: 'sandwiche',
        name: 'Sandwiches',
        img: require('img/icon-hamburguer.png'),
        active: true,
        items: [
          {
            id: 17,
            type: 'sandwiche',
            name: 'Burguer',
            size: '100g',
            description: translate('burgerDesc'),
            price: 14.90,
            img: require('img/icon-hamburguer-1.png'),
            totalItemsPending: 0,
            totalItemsSended: 0,
            totalItemsAdminPending: 0,
            totalItemsAdminSended: 0,
            active: true
          },
          {
            id: 18,
            type: 'sandwiche',
            name: 'Burguer Duo',
            size: '200g',
            description: translate('burgerDuoDesc'),
            price: 16.90,
            img: require('img/icon-hamburguer-2.png'),
            totalItemsPending: 0,
            totalItemsSended: 0,
            totalItemsAdminPending: 0,
            totalItemsAdminSended: 0,
            active: true
          },
          {
            id: 19,
            type: 'sandwiche',
            name: 'Burguer Kids',
            size: '80g',
            description: translate('burgerKidsDesc'),
            price: 12.90,
            img: require('img/icon-hamburguer-4.png'),
            totalItemsPending: 0,
            totalItemsSended: 0,
            totalItemsAdminPending: 0,
            totalItemsAdminSended: 0,
            active: true
          },
          {
            id: 20,
            type: 'sandwiche',
            name: 'Burguer Super',
            size: '250g',
            description: translate('burgerSuperDesc'),
            price: 21.90,
            img: require('img/icon-hamburguer-3.png'),
            totalItemsPending: 0,
            totalItemsSended: 0,
            totalItemsAdminPending: 0,
            totalItemsAdminSended: 0,
            active: true
          },
        ]
      },
      {
        id: 4,
        type: 'juice',
        name: 'Juices',
        img: require('img/icon-drink-3.png'),
        active: true,
        items: [
          {
            id: 12,
            type: 'juice',
            name: translate('juiceOrange'),
            size: '250ml',
            description: translate('juiceOrangeDesc'),
            price: 3.90,
            img: require('img/icon-drink-1.png'),
            totalItemsPending: 0,
            totalItemsSended: 0,
            totalItemsAdminPending: 0,
            totalItemsAdminSended: 0,
            active: true
          },
          {
            id: 13,
            type: 'juice',
            name: translate('juiceRed'),
            size: '350ml',
            description: translate('juiceRedDes'),
            price: 1.90,
            img: require('img/icon-drink-5.png'),
            totalItemsPending: 0,
            totalItemsSended: 0,
            totalItemsAdminPending: 0,
            totalItemsAdminSended: 0,
            active: true
          },
          {
            id: 14,
            type: 'juice',
            name: translate('juiceStraoberry'),
            size: '300ml',
            description: translate('juiceStraoberryDesc'),
            price: 4.90,
            img: require('img/icon-drink-3.png'),
            totalItemsPending: 0,
            totalItemsSended: 0,
            totalItemsAdminPending: 0,
            totalItemsAdminSended: 0,
            active: true
          },
          {
            id: 15,
            type: 'juice',
            name: translate('juiceMoranja'),
            size: '350ml',
            description: translate('juiceMoranjaDesc'),
            price: 6.90,
            img: require('img/icon-drink-4.png'),
            totalItemsPending: 0,
            totalItemsSended: 0,
            totalItemsAdminPending: 0,
            totalItemsAdminSended: 0,
            active: true
          },
          {
            id: 16,
            type: 'juice',
            name: translate('juicePitanga'),
            size: '200ml',
            description: translate('juicePitangaDesc'),
            price: 9.90,
            img: require('img/icon-drink-2.png'),
            totalItemsPending: 0,
            totalItemsSended: 0,
            totalItemsAdminPending: 0,
            totalItemsAdminSended: 0,
            active: true
          },
        ]
      }
    ],
  },
  tables: [
    {
      number: 1,
      users: [],
      items: [],
      status: '', //O - opened, C - Closed, R - Reserved, '' - Empty
    },
    {
      number: 2,
      users: [],
      items: [],
      status: '',
    },
    {
      number: 3,
      users: [],
      items: [],
      status: '',
    },
    {
      number: 4,
      users: [],
      items: [],
      status: '',
    },
    {
      number: 5,
      users: [],
      items: [],
      status: '',
    },
    {
      number: 6,
      users: [],
      items: [],
      status: '',
    },
    {
      number: 7,
      users: [],
      items: [],
      status: '',
    },
    {
      number: 8,
      users: [],
      items: [],
      status: '',
    },
    {
      number: 9,
      users: [],
      items: [],
      status: '',
    },
    {
      number: 10,
      users: [],
      items: [],
      status: '',
    },
    {
      number: 11,
      users: [],
      items: [],
      status: '',
    },
    {
      number: 12,
      users: [],
      items: [],
      status: '',
    },
    {
      number: 13,
      users: [],
      items: [],
      status: '',
    },
    {
      number: 14,
      users: [],
      items: [],
      status: '',
    },
    {
      number: 15,
      users: [],
      items: [],
      status: '',
    },
    {
      number: 16,
      users: [],
      items: [],
      status: '',
    },
    {
      number: 17,
      users: [],
      items: [],
      status: '',
    },
    {
      number: 18,
      users: [],
      items: [],
      status: '',
    },
    {
      number: 19,
      users: [],
      items: [],
      status: '',
    },
    {
      number: 20,
      users: [],
      items: [],
      status: '',
    },
  ],
}
