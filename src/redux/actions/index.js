//User
export const addUser = (payload) => ({
  type: 'ADD_USER',
  payload
})

export const delUser = (payload) => ({
  type: 'DEL_USER',
  payload
})

export const editUser = (payload) => ({
  type: 'EDIT_USER',
  payload
})

export const getUser = (payload) => ({
  type: 'GET_USER',
  payload
})

export const login = (payload) => ({
  type: 'LOGIN',
  payload
})

export const logout = (payload) => ({
  type: 'LOGOUT',
  payload
})

export const settings = (payload) => ({
  type: 'SETTINGS',
  payload
})

//Menu
export const listMenu = (payload) => ({
  type: 'SHOW_MENU',
  payload
})

export const addItemMenu = (payload) => ({
  type: 'ADD_ITEM_MENU',
  payload
})

export const getItemMenu = (payload) => ({
  type: 'LOGIN',
  payload
})

export const editItemMenu = (payload) => ({
  type: 'EDIT_ITEM_MENU',
  payload
})

export const delItemMenu = (payload) => ({
  type: 'DEL_ITEM_MENU',
  payload
})

//Orders
export const addItemOrder = (payload) => ({
  type: 'ADD_ITEM_ORDER',
  payload
})

export const delItemItemOrder = (payload) => ({
  type: 'DEL_ITEM_ORDER',
  payload
})

export const addConfirmedOrder = (payload) => ({
  type: 'ADD_CONFIRMED_ORDER',
  payload
})

export const sendOrder = (payload) => ({
  type: 'SEND_ORDER',
  payload
})

export const confirmeItems = (payload) => ({
  type: 'CONFIRME_ORDER',
  payload
})

export const clearItemsPending = () => ({
  type: 'CLEAR_ITEMS_PENDING'
})

export const clearItemsSended = () => ({
  type: 'CLEAR_ITEMS_SENDED'
})

export const clearItems = () => ({
  type: 'CLEAR_ITEMS'
})

export const clearItemsConfirmedAdmin = () => ({
  type: 'CLEAR_ITEMS_CONFIRMED_ADMIN'
})

export const getOrder = (payload) => ({
  type: 'GET_ORDER',
  payload
})

export const listOrders = (payload) => ({
  type: 'LIST_ORDER',
  payload
})

//account
export const getAccount = (payload) => ({
  type: 'GET_ACCOUNT',
  payload
})

export const closeAccount = (payload) => ({
  type: 'CLOSE_ACCOUNT',
  payload
})

export const openTable = (payload) => ({
  type: 'OPEN_TABLE',
  payload
})

export const cancelTable = () => ({
  type: 'CANCEL_TABLE',
})

//Table
export const addTable = () => ({
  type: 'ADD_TABLE',
})

export const removeTable = (payload) => ({
  type: 'REMOVE_TABLE',
  payload
})

export const setSelectedTable = (payload) => ({
  type: 'SET_SELECTED_TABLE',
  payload
})

export const setTotalSelectedTable = (payload) => ({
  type: 'SET_TOTAL_SELECTED_TABLE',
  payload
})

//Admin
export const setSelectedItemMenu = (payload) => ({
  type: 'SET_SELECTED_ITEM_MENU',
  payload
})

export const delSelectedItemMenu = () => ({
  type: 'DEL_SELECTED_ITEM_MENU',
})

export const addCategory = (payload) => ({
  type: 'ADD_CATEGORY',
  payload
})

export const editCategory = (payload) => ({
  type: 'EDIT_CATEGORY',
  payload
})

export const setCategory = (payload) => ({
  type: 'SET_CATEGORY',
  payload
})

export const deleteCategory = (payload) => ({
  type: 'DELETE_CATEGORY',
  payload
})

export const setItem = (payload) => ({
  type: 'SET_SELECTED_ITEM',
  payload
})

export const addItem = (payload) => ({
  type: 'ADD_ITEM',
  payload
})

export const editItem = (payload) => ({
  type: 'EDIT_ITEM',
  payload
})
