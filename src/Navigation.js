import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Login from './login/Login';
import CreateAccount from './login/CreateAccount';
import Home from './Home';
import Table from './table/Table';
import TabTables from './components/TabTables';
import Settings from './admin/Settings';
import EditCategoryAdmin from './admin/EditCategoryAdmin';
import EditItemAdmin from './admin/EditItemAdmin';
import Account from './account/Account';
import Category from './category/Category';

const HomeStack = createStackNavigator({
  HomeScreen: Home,
}, {
  mode: "card",
  headerMode: "none",
})

const CategoryStack = createStackNavigator({
  CategoryScreen: Category,
}, {
  mode: "card",
  headerMode: "none",
})

const LoginStack = createStackNavigator({
  LoginScreen: Login,
  CreateAccountScreen: CreateAccount
}, {
  mode: "card",
  headerMode: "none",
})

const TableStack = createStackNavigator({
  TableScreen: Table,
  TabTables: TabTables
}, {
  mode: "card",
  headerMode: "none",
})

const SettingsStack = createStackNavigator({
  SettingsScreen: Settings,
  EditCategoryScreenAdmin: EditCategoryAdmin,
  EditItemScreenAdmin: EditItemAdmin,
}, {
  mode: "card",
  headerMode: "none",
})

const AccountStack = createStackNavigator({
  AccountScreen: Account
}, {
  mode: "card",
  headerMode: "none",
})

const AppNavigator = createStackNavigator({
  LoginStack,
  HomeStack,
  CategoryStack,
  TableStack,
  SettingsStack,
  AccountStack,
}, {
  initialRouteName: 'LoginStack',
  mode: "card",
  headerMode: "none",
})

export default createAppContainer(AppNavigator)
