import React, {Component} from 'react';
import {ActivityIndicator, Alert, SafeAreaView, TouchableOpacity} from 'react-native';
import {Button, Header, Icon, Image, Input} from 'react-native-elements';
import {TextInputMask} from 'react-native-masked-text';
import appStyle from '../style/styles.app';
import {translate} from '../locales';
import {Container} from 'native-base';
import {editItem} from '../redux/actions';
import {connect} from 'react-redux';
import ImagePicker from 'react-native-image-picker';

class EditItem extends Component {

  constructor(props) {
    super(props)

    this.state = {
      itemEdited: this.props.item,
      avatarSeleted: '',
    }

  }

  goBack() {
    this.props.navigation.setParams({delItemSelected: true})
    this.props.navigation.goBack()
  }

  getIconBackHeader() {
    return (
      <Icon
        name='ios-arrow-back'
        type='ionicon' color={appStyle.color11}
        onPress={() => {
          this.goBack()
        }}/>
    )
  }

  editValidation() {
    if (this.state.itemEdited.name === ''
      && this.state.itemEdited.description === ''
      && this.state.itemEdited.price === '') {

      Alert.alert(
        'Validation',
        'Please enter name, description and price.',
        [
          {
            text: 'Ok', onPress: () => {
            }
          },
        ],
        {cancelable: false},
      )

    } else if (this.state.itemEdited.name === '') {

      Alert.alert(
        'Validation',
        'Please enter name of menu item',
        [
          {
            text: 'Ok', onPress: () => {
            }
          },
        ],
        {cancelable: false},
      )

    } else if (this.state.itemEdited.description === '') {

      Alert.alert(
        'Validation',
        'Please enter description of menu item',
        [
          {
            text: 'Ok', onPress: () => {
            }
          },
        ],
        {cancelable: false},
      )

    } else if (this.state.itemEdited.price === '') {

      Alert.alert(
        'Validation',
        'Please enter price of menu item',
        [
          {
            text: 'Ok', onPress: () => {
            }
          },
        ],
        {cancelable: false},
      )

    } else if (this.state.itemEdited.size === '') {

      Alert.alert(
        'Validation',
        'Please enter size of menu item',
        [
          {
            text: 'Ok', onPress: () => {
            }
          },
        ],
        {cancelable: false},
      )

    } else {

      if (this.state.itemEdited.name !== ''
        && this.state.itemEdited.description !== ''
        && this.state.itemEdited.size !== ''
        && this.state.itemEdited.price !== '') {

        this.props.editItem(this.state.itemEdited)

        Alert.alert(
          'Success :)',
          'Item Edited!',
          [
            {
              text: 'Ok',
              onPress: () => {
                this.goBack()
              }
            },
          ],
          {cancelable: false},
        )
      } else {
        Alert.alert(
          'Ops :(',
          'We have a problem!',
          [
            {
              text: 'Try again'
            },
          ],
          {cancelable: false},
        )
      }
    }
  }

  selecionarAvatar() {
    const options = {
      title: 'Selecione a capa do evento',
      storageOptions: {
        skipBackup: true,
        path: 'images',
        cameraRoll: true,
        waitUntilSaved: true
      },
    };

    ImagePicker.launchImageLibrary(options, (response) => {
      const source = {uri: 'data:' + response.type + ';base64,' + response.data};

      if (source.uri !== '') {
        let item = this.state.itemEdited
        item.img = source
        this.setState({
          itemEdited: item,
        })
      }
    })
  }

  render() {
    const {item} = this.props
    return (
      <Container style={appStyle.styles.container}>
        <Header
          backgroundColor={appStyle.color2}
          leftComponent={this.getIconBackHeader()}
          centerComponent={{text: translate('titleCoffeeAdmin'), style: {color: appStyle.color11, fontWeight: '600', fontSize: 26}}}
          containerStyle={[appStyle.styles.heightHeader]}
        />
        <SafeAreaView style={[appStyle.styles.bodyApp]}>

          <TouchableOpacity
            onPress={() => {
              this.selecionarAvatar()
            }}>
            <Image
              PlaceholderContent={<ActivityIndicator/>}
              source={item.img} style={[appStyle.styles.imageEditItem, appStyle.styles.marginBottomImg, appStyle.styles.marginTopImg]}/>
          </TouchableOpacity>

          <Input
            containerStyle={[appStyle.styles.marginVertical, appStyle.styles.inputAccount]}
            inputContainerStyle={[{borderBottomWidth: 0,}]}
            inputStyle={{paddingLeft: 10}}
            placeholder={translate('name')}
            onChangeText={(text) => {
              let itemEdited = this.state.itemEdited
              itemEdited.name = text
              this.setState({itemEdited})
            }}
            value={item.name}
          />

          <Input
            containerStyle={[appStyle.styles.marginVertical, appStyle.styles.inputAccount]}
            inputContainerStyle={[{borderBottomWidth: 0,}]}
            inputStyle={{paddingLeft: 10}}
            placeholder={translate('description')}
            onChangeText={(text) => {
              let itemEdited = this.state.itemEdited
              itemEdited.description = text
              this.setState({itemEdited})
            }}
            value={item.description}
          />

          <Input
            containerStyle={[appStyle.styles.marginVertical, appStyle.styles.inputAccount]}
            inputContainerStyle={[{borderBottomWidth: 0,}]}
            inputStyle={{paddingLeft: 10}}
            placeholder={translate('size')}
            onChangeText={(text) => {
              let itemEdited = this.state.itemEdited
              itemEdited.size = text
              this.setState({itemEdited})
            }}
            value={item.size}
          />

          <TextInputMask
            style={[appStyle.styles.marginVertical, appStyle.styles.inputAccount, appStyle.styles.imputMoney]}
            type={'money'}
            placeholder={translate('size')}
            options={{
              precision: 2,
              separator: '.',
              delimiter: ',',
              unit: '',
              suffixUnit: ''
            }}
            value={item.price}
            onChangeText={text => {
              let itemEdited = this.state.itemEdited
              itemEdited.price = parseFloat(text)
              this.setState({itemEdited})
            }}
          />

          <Button
            buttonStyle={[appStyle.styles.btnSave, appStyle.styles.marginBtnLogin]}
            titleStyle={[appStyle.styles.btnTitleSave]}
            title={translate('txtBtnEditImage')}
            onPress={
              () => {
                this.selecionarAvatar()
              }
            }
          />

          <Button
            buttonStyle={[appStyle.styles.btnSave, appStyle.styles.marginBtnLogin]}
            titleStyle={[appStyle.styles.btnTitleSave]}
            title={translate('txtBtnSave')}
            onPress={
              () => {
                this.editValidation()
              }
            }
          />

        </SafeAreaView>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  item: state.menu.items.filter((i) => i.id === state.idItemSelected)[0],
});

const mapDispatchToProps = dispatch => ({
  editItem: (item) => dispatch(editItem(item)),
});

export default connect(mapStateToProps, mapDispatchToProps)(EditItem);
