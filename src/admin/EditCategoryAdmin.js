import React, {Component} from 'react';
import {ActivityIndicator, KeyboardAvoidingView, Platform, SafeAreaView, ScrollView, Switch, Text, TouchableOpacity, View} from 'react-native';
import {Container} from 'native-base';
import {Button, Divider, Header, Icon, Image, Input, ListItem} from 'react-native-elements';
import appStyle from '../style/styles.app';
import {connect} from 'react-redux';
import ImagePicker from 'react-native-image-picker';
import {addCategory, editCategory, setCategory, setItem} from '../redux/actions';

class EditCategoryAdmin extends Component {

  constructor(props) {
    super(props)

    this.state = {
      totalItems: 0,
      visibleModalItem: false,
      numberTable: 0,
      orderModal: '',
      categorySelected: this.props.categorySelected,
      itemSelected: {},
    }
  }

  goToSettings() {
    this.props.navigation.navigate('SettingsScreen', {change: true})
  }

  getIconBackHeader() {
    return (
      <Icon
        name='ios-arrow-back'
        type='ionicon' color={appStyle.color11}
        onPress={() => this.goToSettings()}/>
    )
  }

  setModalVisible() {
    this.setState({visibleModalItem: !this.state.visibleModalItem})
  }

  selecionarAvatar() {
    const options = {
      title: 'Select the image category',
      storageOptions: {
        skipBackup: true,
        path: 'images',
        cameraRoll: true,
        waitUntilSaved: true
      },
    }

    ImagePicker.launchImageLibrary(options, (response) => {
      const source = {uri: 'data:' + response.type + 'base64,' + response.data}

      if (source.uri || source.uri !== '' ) {
        if (!source.uri.includes('undefined')) {
          this.setState({
            categorySelected: {...this.state.categorySelected, img: source}
          })
        }
      }
    })
  }

  getButtons() {
    return(
      <View>
        <Button
          buttonStyle={{
            width: appStyle.width * 0.9,
            backgroundColor: appStyle.color2,
            borderRadius: 50,
            marginVertical: 5
          }}
          title="Add item"
          titleStyle={{color: appStyle.color11,}}
          disabled={this.state.categorySelected.type === ''}
          onPress={() => {
            this.props.setItem()
            this.props.navigation.navigate('EditItemScreenAdmin', {type: this.state.categorySelected.type})
          }}
        />

        <Button
          buttonStyle={{
            width: appStyle.width * 0.9,
            backgroundColor: appStyle.color2,
            borderRadius: 50,
            marginVertical: 5
          }}
          title="Save"
          titleStyle={{color: appStyle.color11,}}
          onPress={() => {
            if (this.state.categorySelected.id === 0) {
              this.props.addCategory(this.state.categorySelected)
              this.props.navigation.navigate('SettingsScreen', {change: true})
            } else {
              this.props.editCategory(this.state.categorySelected)
              this.props.navigation.navigate('SettingsScreen', {change: true})
            }
          }}
        />
      </View>
    )
  }

  render() {
    return (
      <Container style={appStyle.styles.container}>
        <Header
          backgroundColor={appStyle.color2}
          leftComponent={this.getIconBackHeader()}
          centerComponent={
            this.state.categorySelected && this.props.categorySelected.type !== '' ?
              <Text style={[appStyle.styles.titleEditCategory]}>Edit Category</Text>
            :
              <Text style={[appStyle.styles.titleEditCategory]}>Create Category</Text>
          }
          containerStyle={[appStyle.styles.heightHeader]}
        />

      <SafeAreaView style={[appStyle.styles.safeAreaView, appStyle.styles.bodyApp]}>
        <KeyboardAvoidingView
          style={[appStyle.styles.bodyApp]}
          behavior="padding" enabled>
          <TouchableOpacity
            style={[appStyle.styles.viewAvatarEditCategory]}
            onPress={() => {
              this.selecionarAvatar()
            }}>
            {
              this.state.categorySelected && this.state.categorySelected.img !== '' ?
                <Image
                  PlaceholderContent={<ActivityIndicator/>}
                  source={this.state.categorySelected.img} style={[appStyle.styles.imageEditItem]}/>
              :
                <Icon
                  name="md-images"
                  type={'ionicon'}
                  size={110}
                  color={appStyle.color11}
                  iconStyle={{backgroundColor: appStyle.styles.color10}}
                  />
            }
            <Text style={{marginTop: 15}}>Tap here to choose new avatar</Text>
          </TouchableOpacity>

          <Input
            containerStyle={[appStyle.styles.marginVertical, appStyle.styles.inputAccount]}
            inputContainerStyle={[{borderBottomWidth: 0,}]}
            inputStyle={{paddingLeft: 10}}
            placeholder={'Type'}
            onChangeText={(text) => {
              this.setState({categorySelected: {...this.state.categorySelected, type: text}})
            }}
            value={this.state.categorySelected.type}
            />

          <Input
            containerStyle={[appStyle.styles.marginVertical, appStyle.styles.inputAccount]}
            inputContainerStyle={[{borderBottomWidth: 0,}]}
            inputStyle={{paddingLeft: 10}}
            placeholder={'Name'}
            onChangeText={(text) => {
              this.setState({categorySelected: {...this.state.categorySelected, name: text}})
            }}
            value={this.state.categorySelected.name}
            />

            <View style={[appStyle.styles.viewSwitch]}>
              <Text style={[appStyle.styles.labelSwitch]}>
                {
                  this.state.categorySelected ?
                    this.state.categorySelected.active ? 'Active' :  'Inactive'
                  :
                    this.state.categorySelected.active ? 'Active' : 'Inactive'
                }
              </Text>
              <Switch
                onValueChange={(value) => {
                  this.setState({categorySelected: {...this.state.categorySelected, active: !this.state.categorySelected.active}})
                }}
                thumbColor={appStyle.color11}
                trackColor={{false: appStyle.color1, true: appStyle.color2}}
                ios_backgroundColor={appStyle.color6}
                value={this.state.categorySelected.active} />
            </View>

            <View>
              <Text style={[appStyle.styles.titleItems]}>Items</Text>
              <Divider style={[appStyle.styles.dividerSmall]} />
            </View>

            <ScrollView style={{width: appStyle.width * 0.9}}>
              {
                this.state.categorySelected.items.length > 0 ?
                  this.state.categorySelected.items.map(i => (
                    <ListItem
                      key={i.id}
                      leftElement={
                        i.img !== undefined && i.img !== '' ?
                          <Image PlaceholderContent={<ActivityIndicator/>} source={i.img} style={{width: 40, height: 40}}/>
                        :
                          <Icon
                            name="md-images"
                            type={'ionicon'}
                            size={110}
                            color={appStyle.color11}
                            iconStyle={{backgroundColor: appStyle.styles.color10}}
                            />
                      }
                      rightElement={
                        !i.active ?
                          <Icon name={'md-information-circle-outline'} size={26} type={'ionicon'} color={appStyle.color11} />
                        :
                          null
                      }
                      onPress={() => {
                        this.props.setItem(i)
                        this.props.navigation.navigate('EditItemScreenAdmin')
                      }}
                      title={i.name}
                      bottomDivider
                      chevron
                    />
                  ))
                :
                  <Text style={{marginVertical: 10}}>No items</Text>
              }

              {
                Platform.OS === 'android' ?
                  this.getButtons()
                :
                  null
              }

            </ScrollView>
          </KeyboardAvoidingView>

        {
          Platform.OS === 'ios' ?
            this.getButtons()
          :
            null
        }
        </SafeAreaView>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    categorySelected: state.categorySelected,
    categories: state.menu.categories,
    user: state.users.filter((u) => u.logged)[0],
    table: state.tables.filter(t => t.status === 'O')[0],
    tables: state.tables,
    actives: state.categorySelected.items.filter((i) => i.active),
    inactives: state.categorySelected.items.filter((i) => !i.active),
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addCategory: (payload) => dispatch(addCategory(payload)),
    editCategory: (payload) => dispatch(editCategory(payload)),
    setItem: (payload) => dispatch(setItem(payload)),
    setCategory: (payload) => dispatch(setCategory(payload))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditCategoryAdmin)
