import React, {Component} from 'react';
import {SafeAreaView, Text, View} from 'react-native';
import {Container, Content} from 'native-base';
import {connect} from 'react-redux';
import {Header, Icon} from 'react-native-elements';
import appStyle from '../style/styles.app';
import {translate} from '../locales';

class CategoryMenu extends Component {

  constructor(props) {
    super(props)
    this.state = {

    }
  }

  getIconBackHeader() {
    return (
      <Icon
        name='ios-arrow-back'
        type='ionicon' color={appStyle.color11}
        onPress={() => this.props.navigation.navigate('HomeScreen')}/>
    )
  }

  componentDidMount(): void {
  }

  render() {
    const table = this.props.selectedTable
    return (
      <Container>
        <Header
          backgroundColor={appStyle.color2}
          leftComponent={this.getIconBackHeader()}
          centerComponent={{text: translate('titleTable'), style: {color: appStyle.color11, fontWeight: '600', fontSize: 26}}}
          containerStyle={[appStyle.styles.heightHeader]}
        />
        <Content>
          <SafeAreaView style={[]}>
            <View style={[]}>
              <Text style={[appStyle.styles.titleTable]}>asdfafdsda</Text>
            </View>



          </SafeAreaView>
        </Content>

      </Container>
    )
  }
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({})

export default connect(mapStateToProps, mapDispatchToProps)(CategoryMenu)
