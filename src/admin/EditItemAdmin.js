import React, {Component} from 'react';
import {ActivityIndicator, KeyboardAvoidingView, SafeAreaView, Switch, Text, TouchableOpacity, View} from 'react-native';
import {Container} from 'native-base';
import {Button, Header, Icon, Image, Input} from 'react-native-elements';
import appStyle from '../style/styles.app';
import {connect} from 'react-redux';
import ImagePicker from 'react-native-image-picker';
import {addCategory, addItem, editCategory, editItem, setItem} from '../redux/actions';
import NumericInput from '@wwdrew/react-native-numeric-textinput';

class EditItemAdmin extends Component {

  constructor(props) {
    super(props)

    this.state = {
      itemSelected: this.props.itemSelected
    }
  }

  goToEditVategory() {
    this.props.navigation.navigate('EditCategoryScreenAdmin', {change: true})
  }

  getIconBackHeader() {
    return (
      <Icon
        name='ios-arrow-back'
        type='ionicon' color={appStyle.color11}
        onPress={() => this.goToEditVategory()}/>
    )
  }

  selecionarAvatar() {
    const options = {
      title: 'Select the image item',
      storageOptions: {
        skipBackup: true,
        path: 'images',
        cameraRoll: true,
        waitUntilSaved: true
      },
    }

    ImagePicker.launchImageLibrary(options, (response) => {
      const source = {uri: 'data:' + response.type + 'base64,' + response.data}

      if (source.uri !== '' || source.uri) {
        this.setState({
          itemSelected: {...this.state.itemSelected, img: source}
        })
      }
    })
  }

  render() {
    return (
      <Container style={appStyle.styles.container}>
        <Header
          backgroundColor={appStyle.color2}
          leftComponent={this.getIconBackHeader()}
          centerComponent={
            this.state.itemSelected.id > 0 ?
              <Text style={{color: appStyle.color11, fontWeight: '600', fontSize: 26}}>Edit Item</Text>
            :
              <Text style={{color: appStyle.color11, fontWeight: '600', fontSize: 26}}>Create Item</Text>
          }
          containerStyle={[appStyle.styles.heightHeader]}
        />

      <SafeAreaView style={[appStyle.styles.safeAreaView, appStyle.styles.bodyApp]}>
        <KeyboardAvoidingView
          style={[appStyle.styles.bodyApp]}
          behavior="padding" enabled>
          <TouchableOpacity
            style={[appStyle.styles.viewAvatarEditCategory]}
            onPress={() => {
              this.selecionarAvatar()
            }}>
            {
              this.state.itemSelected && this.state.itemSelected.img ?
                <Image
                  PlaceholderContent={<ActivityIndicator/>}
                  source={this.state.itemSelected.img} style={[appStyle.styles.imageEditItem]}/>
              :
                this.state.itemSelected.img === '' ?
                  <Icon
                    name="md-images"
                    type={'ionicon'}
                    size={110}
                    color={appStyle.color11}
                    iconStyle={{backgroundColor: appStyle.styles.color10}}
                    />
                :
                  <Image
                    PlaceholderContent={<ActivityIndicator/>}
                    source={this.state.itemSelected.img} style={[appStyle.styles.imageEditItem]}/>
            }
            <Text style={{marginTop: 15}}>{'Tap here to choose new avatar'}</Text>
          </TouchableOpacity>

          <Input
            containerStyle={[appStyle.styles.marginVertical, appStyle.styles.inputAccount, {backgroundColor: appStyle.color12}]}
            inputContainerStyle={[{borderBottomWidth: 0}]}
            inputStyle={{paddingLeft: 10}}
            placeholder={'Type'}
            editable={false}
            value={this.props.navigation.getParam('type') ? this.props.navigation.getParam('type') : this.props.itemSelected.type}
            />

          <Input
            containerStyle={[appStyle.styles.marginVertical, appStyle.styles.inputAccount]}
            inputContainerStyle={[{borderBottomWidth: 0,}]}
            inputStyle={{paddingLeft: 10}}
            placeholder={'Name'}
            onChangeText={(text) => {
              this.setState({itemSelected: {...this.state.itemSelected, name: text}})
            }}
            value={this.state.itemSelected.name}
            />

          <Input
            containerStyle={[appStyle.styles.marginVertical, appStyle.styles.inputAccount]}
            inputContainerStyle={[{borderBottomWidth: 0,}]}
            inputStyle={{paddingLeft: 10}}
            placeholder={'Description'}
            onChangeText={(text) => {
              this.setState({itemSelected: {...this.state.itemSelected, description: text}})
            }}
            value={this.props.itemSelected.description ? this.state.itemSelected.description : this.state.itemSelected.description}
            />

          <Input
            containerStyle={[appStyle.styles.marginVertical, appStyle.styles.inputAccount]}
            inputContainerStyle={[{borderBottomWidth: 0,}]}
            inputStyle={{paddingLeft: 10}}
            placeholder={'Size'}
            onChangeText={(text) => {
              this.setState({itemSelected: {...this.state.itemSelected, size: text}})
            }}
            value={this.props.itemSelected.size ? this.state.itemSelected.size : this.state.itemSelected.size}
            />

          <NumericInput
            style={[
              appStyle.styles.marginVertical,
              appStyle.styles.inputAccount,
              {paddingLeft: 20, height: 40, width: appStyle.width * 0.9, fontSize: 18}]}
            type='decimal'
            decimalPlaces={2}
            placeholder={'Price'}
            onUpdate={(text) => {
              this.setState({itemSelected: {...this.state.itemSelected, price: text}})
            }}
            value={this.state.itemSelected.price ? this.state.itemSelected.price : this.state.itemSelected.price}
            />

            <View style={[appStyle.styles.viewSwitch]}>
              <Text style={[appStyle.styles.labelSwitch]}>
                {
                  this.state.itemSelected ?
                    this.state.itemSelected.active ? 'Active' :  'Inactive'
                  :
                    this.state.itemSelected.active ? 'Active' : 'Inactive'
                }
              </Text>
              <Switch
                onValueChange={(value) => {
                  if (this.state.itemSelected) {
                    this.setState({itemSelected: {...this.state.itemSelected, active: value}})
                  } else {
                    this.setState({item: {...this.state.item, active: value}})
                  }
                }}
                thumbColor={appStyle.color11}
                trackColor={{false: appStyle.color1, true: appStyle.color2}}
                ios_backgroundColor={appStyle.color6}
                value={this.state.itemSelected ? this.state.itemSelected.active : this.state.itemSelected.active} />
            </View>
          </KeyboardAvoidingView>

          <Button
            buttonStyle={{
              width: appStyle.width * 0.9,
              backgroundColor: appStyle.color2,
              borderRadius: 50,
              marginVertical: 5
            }}
            title="Add+"
            titleStyle={{color: appStyle.color11,}}
            disabled={
              this.state.itemSelected.id > 0 ?
                true
              :
                this.state.itemSelected.name === ''
            }
            onPress={() => {
              this.props.addItem(this.state.itemSelected)
              this.props.setItem()
              this.setState({
                itemSelected: {
                  ...this.props.itemSelected,
                  type: this.props.navigation.getParam('type') ? this.props.navigation.getParam('type') : this.props.itemSelected.type,
                }
              })
            }}
          />

          <Button
            buttonStyle={{
              width: appStyle.width * 0.9,
              backgroundColor: appStyle.color2,
              borderRadius: 50,
              marginVertical: 5
            }}
            title="Save"
            titleStyle={{color: appStyle.color11,}}
            disabled={this.state.itemSelected.name === ''}
            onPress={() => {
              if (this.props.itemSelected.id > 0) {
                this.props.editItem(this.state.itemSelected)
                this.props.setItem()
                this.props.navigation.navigate('EditCategoryScreenAdmin', {change: true})
              } else if (this.props.itemSelected.id === 0) {
                this.props.addItem(this.state.itemSelected)
                this.props.setItem()
                this.props.navigation.navigate('EditCategoryScreenAdmin', {change: true})
              } else {
                this.props.setItem()
                this.props.navigation.navigate('EditCategoryScreenAdmin', {change: true})
              }
            }}
          />
        </SafeAreaView>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    categorySelected: state.categorySelected,
    itemSelected: state.itemSelected,
    categories: state.menu.categories,
    user: state.users.filter((u) => u.logged)[0],
    table: state.tables.filter(t => t.status === 'O')[0],
    tables: state.tables,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addCategory: (payload) => dispatch(addCategory(payload)),
    editCategory: (payload) => dispatch(editCategory(payload)),
    setItem: (payload) => dispatch(setItem(payload)),
    addItem: (payload) => dispatch(addItem(payload)),
    editItem: (payload) => dispatch(editItem(payload)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditItemAdmin)
