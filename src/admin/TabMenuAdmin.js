import React, {Component} from 'react';
import {ActivityIndicator, SafeAreaView, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {Divider, Icon, Image, ListItem} from 'react-native-elements';
import {connect} from 'react-redux';
import appStyle from '../style/styles.app';

class TabMenuAdmin extends Component {

  constructor(props) {
    super(props)
    this.state = {
    }
  }

  setModalVisible() {
    this.setState({modalVisible: !this.state.modalVisible})
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1, width: appStyle.width, height: appStyle.height}}>
        <ScrollView>
          {
            this.props.actives.length > 0 ?
              <View>
                <Text style={[appStyle.styles.titleCategory]}>Actives categories</Text>
                <Divider style={[appStyle.styles.dividerLarge]} />
              </View>
            :
              null
          }
          <View>
            {
              this.props.categories.map(c => {
                if (c.active) {
                  return(
                    <ListItem
                      key={c.id}
                      leftElement={
                        c.img !== undefined && c.img !== '' ?
                          <Image PlaceholderContent={<ActivityIndicator/>} source={c.img} style={{width: 40, height: 40}}/>
                        :
                          <Icon
                            name="md-images"
                            type={'ionicon'}
                            size={40}
                            color={appStyle.color11}
                            iconStyle={{backgroundColor: appStyle.styles.color10}}
                            />
                      }
                      onPress={() => {
                        this.props.navigate(c)
                      }}
                      title={c.name}
                      bottomDivider
                      chevron
                    />
                  )
                }
            })
          }
          </View>

          {
            this.props.inActives.length > 0 ?
              <View>
                <Text style={[appStyle.styles.titleCategory]}>Inactives categories</Text>
                <Divider style={[appStyle.styles.dividerLarge]} />
              </View>
            :
              null
          }

          <View>
            {
              this.props.categories.map(c => {
                if (!c.active) {
                  return(
                    <ListItem
                      key={c.id}
                      leftElement={
                        c.img !== undefined && c.img !== '' ?
                          <Image PlaceholderContent={<ActivityIndicator/>} source={c.img} style={{width: 40, height: 40}}/>
                        :
                          <Icon
                            name="md-images"
                            type={'ionicon'}
                            size={40}
                            color={appStyle.color11}
                            iconStyle={{backgroundColor: appStyle.styles.color10}}
                            />
                      }
                      onPress={() => {
                        this.props.navigate(c)
                      }}
                      title={c.name}
                      bottomDivider
                      chevron
                    />
                  )
                }
            })
          }
          </View>
        </ScrollView>

        <View style={[appStyle.styles.centerElements, appStyle.styles.rowElements]}>
          <TouchableOpacity
            style={[appStyle.styles.cardAddTable]}
            onPress={() => {
              this.props.setCategory()
              this.props.navigate()
            }}>
            <Icon
              name='ios-add'
              type='ionicon'
              color={appStyle.color11}
              size={36}
            />
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    )
  }
}

const mapStateToProps = (state) => ({
  actives: state.menu.categories.filter((c) => c.active),
  inActives: state.menu.categories.filter((c) => !c.active),
})

export default connect(mapStateToProps)(TabMenuAdmin)
