import React, {Component} from 'react';
import {SafeAreaView, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import appStyle from '../style/styles.app';
import {Icon} from 'react-native-elements';
import {addTable, removeTable} from '../redux/actions';

class TabTablesAdmin extends Component {

  constructor(props) {
    super(props)
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1, width: appStyle.width, height: appStyle.height}}>
        <ScrollView>
          <View style={appStyle.styles.cards}>

            {
              this.props.tables.map(t => (
                <View
                  key={t.number}
                  style={[appStyle.styles.cardTable]}>
                  <Text style={[appStyle.styles.textCardTable]}>{t.number}</Text>
                </View>
              ))
            }

          </View>
        </ScrollView>

        {
          this.props.user.userAdmin ?
            <View style={[appStyle.styles.centerElements, appStyle.styles.rowElements]}>
              <TouchableOpacity
                style={[appStyle.styles.cardAddTable]}
                onPress={() => {
                  this.props.removeTable()
                  this.forceUpdate()
                }}>
                <Icon
                  name='ios-remove'
                  type='ionicon'
                  color={appStyle.color11}
                  size={36}
                />
              </TouchableOpacity>

              <TouchableOpacity
                style={[appStyle.styles.cardAddTable]}
                onPress={() => {
                  this.props.addTable()
                  this.forceUpdate()
                }}>
                <Icon
                  name='ios-add'
                  type='ionicon'
                  color={appStyle.color11}
                  size={36}
                />
              </TouchableOpacity>
            </View>
          :
            null
        }

      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.users.filter((u) => u.logged)[0],
  tables: state.tables
})

const mapDispatchToProps = (dispatch) => ({
  addTable: (table) => dispatch(addTable(table)),
  removeTable: (table) => dispatch(removeTable(table))
})

export default connect(mapStateToProps, mapDispatchToProps)(TabTablesAdmin)
