import React, {Component} from 'react';
import {ActivityIndicator, Platform, SafeAreaView, Text, View} from 'react-native';
import {connect} from 'react-redux';
import appStyle from '../style/styles.app';
import {Header, Icon, Image} from 'react-native-elements';
import {translate} from '../locales';
import {Container, Tab, TabHeading, Tabs} from 'native-base';
import TabOrders from '../components/TabOrders';
import TabTablesAdmin from './TabTablesAdmin';
import TabMenuAdmin from './TabMenuAdmin';
import {deleteCategory, logout, setCategory, setSelectedTable, settings} from '../redux/actions';

class Settings extends Component {

  constructor(props) {
    super(props)
    this.state = {
      modalVisible: false
    }
  }

  navigateEditCategoryScreen(category) {
    if (category) {
      this.props.setCategory(category)
      this.props.navigation.navigate('EditCategoryScreenAdmin')
    } else {
      this.props.setCategory(undefined)
      this.props.navigation.navigate('EditCategoryScreenAdmin')
    }

  }

  goToTableAdmin(table) {
    this.props.navigation.navigate('TableScreenAdmin', {table: table})
  }

  goToLogin() {
    this.props.navigation.navigate('LoginScreen')
  }

  logout() {
    this.props.logout()
    this.goToLogin()
  }

  getIconBackHeader() {
    return (
      <Icon
        name='ios-arrow-back'
        type='ionicon' color={appStyle.color11}
        onPress={() => {
          let payload = {
            id: this.props.user.id,
            setting: false
          }
          this.props.setSettings(payload)
          this.props.navigation.navigate('HomeScreen', {changeTables: true})
        }}/>
    )
  }

  render() {
    return (
      <Container style={appStyle.styles.container}>
        <Header
          leftComponent={this.getIconBackHeader()}
          backgroundColor={appStyle.color2}
          centerComponent={{text: 'Menu Configuration', style: [appStyle.styles.titleHeader]}}
          centerContainerStyle={{width: appStyle.width}}
          containerStyle={[appStyle.styles.heightHeader]}
        />
        <SafeAreaView style={{flex: 1, width: appStyle.width, height: appStyle.height}}>

          <View>

            <View style={[appStyle.styles.viewAppLogo]}>
              <Image
                PlaceholderContent={<ActivityIndicator/>}
                source={require('../img/logo-coffee.png')} style={[appStyle.styles.logo]}/>

              <Text style={[appStyle.styles.textHeader]}>{translate('slogan')}</Text>

            </View>

          </View>

          <Tabs tabBarUnderlineStyle={{backgroundColor: appStyle.color4,}}>
            <Tab
              style={{backgroundColor: appStyle.color10}}
              heading={
                <TabHeading
                  activeTextStyle={{color: appStyle.color10}}
                  style={{backgroundColor: appStyle.color10,}}>
                  <Text style={{color: appStyle.color11, fontSize: 18}}>{translate('menu')}</Text>
                </TabHeading>
              }>
              <TabMenuAdmin
                navigate={(screen) => this.navigateEditCategoryScreen(screen)}
                categories={this.props.categories}
                setCategory={() => {
                  this.props.setCategory({
                    id: this.props.categories.length > 0 ? this.props.categories.length + 1 : 0,
                    type: '',
                    name: '',
                    img: '',
                    active: true,
                    items: [],
                  }
                )}}
                deleteCategory={(category) => this.props.deleteCategory(category)}
              />
            </Tab>

            {
              this.props.user.userAdmin ?
                <Tab
                  style={{backgroundColor: appStyle.color10}}
                  heading={
                    <TabHeading style={{backgroundColor: appStyle.color10,}}>
                      <Text style={{color: appStyle.color11, fontSize: 18}}>{translate('tables')}</Text>
                    </TabHeading>
                  }>
                  <TabTablesAdmin />
                </Tab>
              :
                <Tab
                  style={{backgroundColor: appStyle.color10}}
                  heading={
                    <TabHeading style={{backgroundColor: appStyle.color10,}}>
                      <Text style={{color: appStyle.color11, fontSize: 18}}>{translate('orders')}</Text>
                    </TabHeading>
                  }>
                  <TabOrders />
                </Tab>
            }
          </Tabs>

        </SafeAreaView>
      </Container>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.users.filter((u) => u.logged)[0],
  tables: state.tables,
  categories: state.menu.categories,
})

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(logout()),
  setSettings: (payload) => dispatch(settings(payload)),
  setCategory: (payload) => dispatch(setCategory(payload)),
  deleteCategory: (payload) => dispatch(deleteCategory(payload)),
  setSelectedTable: (table) => dispatch(setSelectedTable(table)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Settings)
