import React, {Component} from 'react';
import {Alert, SafeAreaView, Text, View} from 'react-native';
import {Container, Content} from 'native-base';
import {connect} from 'react-redux';
import {Header, Icon} from 'react-native-elements';
import appStyle from '../style/styles.app';
import {translate} from '../locales';
import CardPendingOrder from '../components/CardPendingOrder';
import CardConsumption from '../components/CardConsumption';

class AddCategoryMenu extends Component {

  constructor(props) {
    super(props)
    this.state = {
      ordersSended: this.props.selectedTable.orders.filter(o => o.status === 'S' && o.author === 'A'),
      ordersPending: this.props.selectedTable.orders.filter(o => o.status === 'P' && o.author === 'A'),
      itemsSended: []
    }
  }

  getIconBackHeader() {
    return (
      <Icon
        name='ios-arrow-back'
        type='ionicon' color={appStyle.color11}
        onPress={() => this.props.navigation.navigate('HomeScreen')}/>
    )
  }

  finishOrder() {

    let message = ''
    let totalPriceItems = 0

    this.props.selectedTable.orders.map((o) => {
      if (o.status === 'P' && o.author === 'A') {
        o.items.map(i => {
          message += i.totalItemsAdminPending + ' ' + i.name + '\n'
        })
      }
    })

    this.props.selectedTable.orders.map((o) => {
      if (o.status === 'P' && o.author === 'A') {
        o.items.map(i => {
          if (i.totalItemsAdminPending > 0) {
            totalPriceItems += i.price * i.totalItemsAdminPending
          }
        })
      }
    })

    message += translate('money') + ' ' + totalPriceItems.toFixed(2)

    Alert.alert(
      translate('textAlertConfirmOrder'),
      message,
      [
        {
          text: translate('textAlertCancelItem'),
          onPress: () => {
          },
          style: 'cancel',
        },
        {
          text: translate('textAlertOkItem'),
          onPress: () => {
            this.props.selectedTable.orders.map((o) => {
              if (o.status === 'P' && o.author === 'A') {
                o.status = 'S'

                o.items.map(i => {
                  i.totalItemsAdminSending = i.totalItemsAdminPending
                  i.totalItemsAdminPending = 0
                })

                let allItems = []
                allItems.push(this.state.itemsSended)
                allItems.push(o.items)
                this.setState({itemsSended: allItems})
              }
            })


            this.setState({
              ordersPending: [],
              ordersSended: this.props.selectedTable.orders.filter(o => o.status === 'S' && o.author === 'A')}),
            this.forceUpdate()
          },
        },
      ],
      {cancelable: false},
    )
  }

  cancelPending() {
    Alert.alert(
      translate('closeAccount'),
      'Are you sure you want to cancel?\n',
      [
        {
          text: translate('textAlertCancelItem'),
          onPress: () => {
          },
          style: 'cancel',
        },
        {
          text: translate('txtBtnOk'),
          onPress: () => {

            this.state.ordersPending[0].items.map(i => {
              i.totalItemsAdminPending = 0
              i.totalItemsPending = 0
            })

            this.state.ordersPending = []
            let newOrders = this.props.selectedTable.orders.filter(o => (o.status === 'P' && o.author !== 'A'))
            this.props.selectedTable.orders = newOrders
            this.forceUpdate()
          },
          style: 'ok',
        },
      ],
      {cancelable: false},
    )
  }

  closeConsumption() {
    Alert.alert(
      translate('closeAccount'),
      'We are grateful for your preference.\n',
      [
        {
          text: translate('textAlertCancelItem'),
          onPress: () => {
          },
          style: 'cancel',
        },
        {
          text: translate('txtBtnOk'),
          onPress: () => {
            this.props.selectedTable.orders.map(o => {
              o.items.map(i => {
                i.totalItemsAdminPending = 0
                i.totalItemsAdminSending = 0
                i.totalItemsPending = 0
                i.totalItemsSended = 0
              })

              o.items = []
            })

            this.props.selectedTable.orders = []
            this.setState({ordersPending: [], ordersSended: [], itemsSended: []})
            this.forceUpdate()
          },
          style: 'ok'
        },
      ],
      {cancelable: false},
    )

  }

  getOrdersSended() {
    let itemsSended = []

    this.props.tables.map(t => {
      if (t.orders.length > 0) {
        t.orders.map(o => {
          if (o.status === 'S') {
            o.items.map(i => {
              itemsSended.push(i)
            })
          }
        })
      }
    })

    this.setState({itemsSended})
  }

  componentDidMount(): void {
    this.getOrdersSended()
  }

  render() {
    const table = this.props.selectedTable
    return (
      <Container>
        <Header
          backgroundColor={appStyle.color2}
          leftComponent={this.getIconBackHeader()}
          centerComponent={{text: translate('titleTable'), style: {color: appStyle.color11, fontWeight: '600', fontSize: 26}}}
          containerStyle={[appStyle.styles.heightHeader]}
        />
        <Content>
          <SafeAreaView style={[appStyle.styles.container]}>
            <View style={[appStyle.styles.viewTitleTable]}>
              <Text style={[appStyle.styles.titleTable]}>{table.number}</Text>
            </View>

            {
              this.state.ordersPending.length === 0 && this.state.ordersSended.length === 0 ?
                <Text style={[appStyle.textColorDefault, appStyle.styles.textNoOrder]}>{translate('noOrder')}</Text>
                :
                null
            }

            {
              this.state.ordersPending.length > 0 ?
                <CardPendingOrder
                  items={this.state.ordersPending[0].items}
                  finishOrder={() => this.finishOrder()}
                  cancel={() => this.cancelPending()}
                />
                :
                null
            }

            {
              this.state.ordersSended.length > 0 ?
                <CardConsumption
                  items={this.state.ordersSended[0].items}
                  closeConsumption={() => this.closeConsumption()}
                />
                :
                null
            }

          </SafeAreaView>
        </Content>

      </Container>
    )
  }
}

const mapStateToProps = state => ({
  selectedTable: state.selectedTable,
  tables: state.tables
})

const mapDispatchToProps = dispatch => ({})

export default connect(mapStateToProps, mapDispatchToProps)(AddCategoryMenu)
