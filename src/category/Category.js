import React, {Component} from 'react';
import {ActivityIndicator, Alert, Image, Platform, SafeAreaView, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {Container, Content} from 'native-base';
import {Divider, Header, Icon} from 'react-native-elements';
import BtnAddItems from '../components/BtnAddItems';
import BtnLoginTable from '../components/BtnLoginTable';
import appStyle from '../style/styles.app';
import Fade from '../components/Fade';
import {translate} from '../locales';
import {connect} from 'react-redux';
import {sendOrder} from '../redux/actions';
import OpenModal from '../components/OpenTableModal';

class Category extends Component {

  constructor(props) {
    super(props)

    this.state = {
      table: this.props.table,
      category: this.props.category,
      totalItemsPending: 0,
      totalItemsSended: 0,
      modalOpenTableVisible: false,
      modalOpenOrderVisible: false,
      numberTable: 0,
      orderModal: '',
      selectedTable: 0,
    }
  }

  goToHome() {
    this.props.navigation.navigate('HomeScreen', {tableNumber: this.props.table, change: 'zxcvasdf'})
  }

  setModalOpenTableVisible() {
    this.setState({modalOpenTableVisible: !this.state.modalOpenTableVisible})
  }

  openTable() {
    this.props.openTable(this.state.numberTable)
    this.setModalOpenTableVisible()
  }

  calculateTotalPending() {
    let total = 0

    if (!this.props.user.userAdmin) {
      total = this.state.category.items.reduce((reducer, i) => (
        {totalItemsPending: reducer.totalItemsPending + i.totalItemsPending}
      )).totalItemsPending
    } else {
      total = this.state.category.items.reduce((reducer, i) => (
        {totalItemsAdminPending: reducer.totalItemsAdminPending + i.totalItemsAdminPending}
      )).totalItemsAdminPending
    }
    this.setState({totalItemsPending: total})
  }

  plusItem(item) {
    this.state.category.items.map(i => {
      if (item.id === i.id && !this.props.user.userAdmin) {
        i.totalItemsPending++
      } else if (item.id === i.id) {
        i.totalItemsAdminPending++
      }
    })
  }

  addItem(item) {

    if (!this.props.user.userAdmin) {

      this.plusItem(item)
      let table = this.props.table

      if (table.items.length === 0) {
        let order = {
          status: 'P', //P Pendending, S Sended
          items: [item],
          tableOrder: table.number,
          author: 'U' //U - User, A - Admin
        }
        table.items.push(order)
      } else {
        table.items.map(o => {
          if (o.status === 'P' && !o.items.includes(item)) {
            o.items.push(item)
          }
        })
      }
    } else {

      if (this.state.selectedTable === 0) {
        Alert.alert(
          'Validation',
          `Please select one table to add item.`,
          [
            {
              text: 'Ok', onPress: () => {
              }
            },
          ],
          {cancelable: false},
        )
      } else {

        this.plusItem(item)

        this.props.tables.map((t) => {
          if (this.state.selectedTable === t.number) {
            if (t.items.length === 0) {
              let order = {
                status: 'P', //P Pendending, S Sended
                items: [item],
                tableOrder: t.number,
                author: 'A'
              }
              t.items.push(order)
            } else {
              t.items.map(o => {
                if (o.status === 'P' && !o.items.includes(item)) {
                  o.items.push(item)
                }
              })
            }
          }
        })
      }
    }
    this.calculateTotalPending()
  }

  subItem(item) {
    this.state.category.items.map(i => {
      if (item.id === i.id && !this.props.user.userAdmin) {
        i.totalItemsPending--
        this.setState({totalItemsPending: (this.state.totalItemsPending - 1)})
      } else if (item.id === i.id) {
        i.totalItemsAdminPending--
        this.setState({totalItemsPending: (this.state.totalItemsPending - 1)})
      }
    })
  }

  removeItem(item) {
    let table = null

    if (!this.props.user.userAdmin) {
      table = this.props.table
    } else {
      table = this.props.tables.filter(t => t.number === this.state.selectedTable)[0]
    }

    this.subItem(item)

    if (table.items.length > 0) {
      table.items.map(o => {
        let itemsUpdated = {}

        if (o.status === 'P') {
          if (this.props.user.userAdmin) {
            itemsUpdated = o.items.filter(i => {
              if (i.totalItemsAdminPending > 0) {
                return i
              }
            })
          } else {
            itemsUpdated = o.items.filter(i => {
              if (i.totalItemsPending > 0) {
                return i
              }
            })
          }

          o.items = itemsUpdated
        }
      })

      this.calculateTotalPending()

    } else {
      table.items = []
    }
  }


  clearTotalPending() {
    this.state.category.items.map(c => {
      c.totalItemsPending = 0
      this.setState({totalItemsPending: 0})
    })
  }

  cancel() {
    this.clearTotalPending()
    this.goToHome()
  }

  getIconBackHeader() {
    return (
      <Icon
        name='ios-arrow-back'
        type='ionicon' color={appStyle.color11}
        iconStyle={appStyle.styles.iconBack}
        onPress={() => this.goToHome()}/>
    )
  }

  getRightComponent() {
    if ((this.props.table && this.props.table.status === 'O') && !this.props.user.userAdmin) {
      return (
        <View style={[appStyle.styles.viewNumberTable, appStyle.styles.centerElements]}>
          <Text style={[appStyle.styles.numberTable]}>{this.props.table.number}</Text>
        </View>
      )
    }
  }

  render() {
    return (
      <Container style={appStyle.styles.container}>
        <Header
          backgroundColor={appStyle.color2}
          leftComponent={this.getIconBackHeader()}
          centerComponent={{
            text: this.state.category.name, style: {color: appStyle.color11, fontWeight: '600', fontSize: 26}
          }}
          rightComponent={this.getRightComponent()}
          containerStyle={[appStyle.styles.heightHeader]}
        />
        <SafeAreaView style={{flex: 1, width: appStyle.width, height: appStyle.height, marginBottom: 10}}>

          <Content>
            {
              this.props.user.userAdmin ?
                <View>
                  <ScrollView
                    horizontal={true}
                  >
                    {
                      this.props.tables.map(t => (
                        <TouchableOpacity
                          key={t.number}
                          onPress={() => {
                            if (t.number === this.state.selectedTable) {
                              this.setState({selectedTable: 0})
                            } else {
                              this.setState({selectedTable: t.number})
                              this.props.category.items.map(i => {
                                i.totalItemsPending = 0
                              })
                              this.setState({totalItemsPending: 0})
                            }
                            this.forceUpdate()

                          }}
                          style={[
                            appStyle.styles.tableModal,
                            {backgroundColor: this.state.selectedTable === t.number ? appStyle.color2 : appStyle.color11}
                          ]}
                        >
                          <Text
                            style={[
                              appStyle.styles.textTableModal,
                              {color: this.state.selectedTable === t.number ? appStyle.color11 : appStyle.color2}]}>
                            {t.number}
                          </Text>
                        </TouchableOpacity>
                      ))
                    }
                  </ScrollView>

                  <Divider style={{backgroundColor: 'gray'}}/>
                </View>
                :
                null
            }

            {
              this.state.category.items.map(i => (
                <View style={[appStyle.styles.cardMenu]} key={i.id}>
                  <Image
                    PlaceholderContent={<ActivityIndicator/>}
                    source={i.img}
                    style={[appStyle.styles.imageItem]}/>

                  <View style={[appStyle.styles.viewItemLista]}>
                    <Text style={appStyle.styles.tituloItemLista}>{i.name} - {i.size}</Text>
                    <Text style={appStyle.styles.infoItemLista}>{i.description}</Text>
                    <Text style={appStyle.styles.precoItemLista}>{translate('money')} {i.price.toFixed(2)}</Text>
                  </View>

                  <View style={[appStyle.styles.viewCart]}>
                    <TouchableOpacity
                      style={[appStyle.styles.btnRemoveCart]}
                      onPress={() => {
                        this.removeItem(i)
                        this.forceUpdate()
                      }}>
                      {
                        i.totalItemsPending > 0 || i.totalItemsAdminPending > 0 ?
                          <Text style={[appStyle.styles.txtBtnRemoveCart]}>-</Text>
                          :
                          null
                      }
                    </TouchableOpacity>

                    {
                      (this.props.table && this.props.table.status === 'O') || (this.props.user && this.props.user.userAdmin) ?
                        <TouchableOpacity
                          style={[appStyle.styles.btnAddCart]}
                          onPress={() => {
                            this.addItem(i)
                            this.forceUpdate()
                          }}>
                          {
                            i.totalItemsPending > 0 || i.totalItemsAdminPending > 0 ?
                              <Text style={[appStyle.styles.txtBtnAddCart]}>
                                {this.props.user.userAdmin ? i.totalItemsAdminPending : i.totalItemsPending}
                              </Text>
                              :
                              <Text style={[appStyle.styles.txtBtnAddCart]}>+</Text>
                          }
                        </TouchableOpacity>
                        :
                        null
                    }
                  </View>
                </View>
              ))
            }

          </Content>

          {
            (!this.props.table || this.props.table.status === '') && !this.props.user.userAdmin ?
              <Fade visible={true}>
                <View style={{
                  alignItems: 'center',
                  justifyContent: 'flex-end',
                }}>
                  <BtnLoginTable
                    setModalVisible={() => this.setModalOpenTableVisible()}
                    showButton={true}/>
                </View>
              </Fade>
              :
              null
          }

          {
            this.state.totalItemsPending > 0 || this.props.user.userAdmin ?
              <Fade visible={true}>
                <View style={{
                  alignItems: 'center',
                  justifyContent: 'flex-end',
                }}>
                  <BtnAddItems
                    label={translate('txtBtnOk')}
                    action={() => this.goToHome()}
                    showButton={true}/>
                </View>
              </Fade>
              :
              <Fade visible={false}>
                <View style={{
                  alignItems: 'center',
                  justifyContent: 'flex-end',
                }}>
                  <BtnAddItems
                    label={translate('txtBtnOk')}
                    action={() => this.goToHome()}
                    showButton={true}/>
                </View>
              </Fade>
          }

          <OpenModal
            closeModal={() => this.setModalOpenTableVisible()}
            modalVisible={() => this.state.modalOpenTableVisible}
          />

        </SafeAreaView>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    category: state.menu.categories.filter((i) => i.type === state.categorySelected.type)[0],
    user: state.users.filter((u) => u.logged)[0],
    table: state.tables.filter(t => t.status === 'O')[0],
    tables: state.tables,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getTotalPending: (order) => dispatch(sendOrder(order)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Category)
