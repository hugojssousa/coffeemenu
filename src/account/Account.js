import React, {Component} from 'react';
import {ActivityIndicator, Alert, SafeAreaView, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import appStyle from '../style/styles.app';
import {Button, Header, Icon, Image, Input} from 'react-native-elements';
import {translate} from '../locales';
import {Container} from 'native-base';
import {editUser, logout} from '../redux/actions';
import ImagePicker from 'react-native-image-picker';
import util from '../util/Util';

class Account extends Component {

  constructor(props) {
    super(props)

    this.state = {
      localUser: this.props.user
    }
  }

  accountValidation() {
    if (
      this.state.localUser.email.trim() === ''
      && this.state.localUser.password.trim() === ''
      && this.state.localUser.name.trim() === '') {

      Alert.alert(
        'Validation',
        'Please enter your name, e-mail and password',
        [
          {text: 'Try again'},
        ],
        {cancelable: false},
      )

    } else if (this.state.localUser.email.trim() === '') {

      Alert.alert(
        'Validation',
        'Please enter your e-mail',
        [
          {
            text: 'Try again', onPress: () => {
            }
          },
        ],
        {cancelable: false},
      )

    } else if (!util.emailValidate(this.state.localUser.email)) {

      Alert.alert(
        'Validation',
        'Please enter a e-mail valide',
        [
          {
            text: 'Try again', onPress: () => {
            }
          },
        ],
        {cancelable: false},
      )

    } else if (this.state.localUser.password.trim() === '') {

      Alert.alert(
        'Validation',
        'Please enter your password',
        [
          {
            text: 'Tray again', onPress: () => {
            }
          },
        ],
        {cancelable: false},
      )

    } else if (this.state.localUser.name.trim() === '') {

      Alert.alert(
        'Validation',
        'Please enter your name',
        [
          {
            text: 'Tray again', onPress: () => {
            }
          },
        ],
        {cancelable: false},
      )

    } else {
      this.props.editUser(this.state.localUser)

      if (this.props.user !== null) {
        Alert.alert(
          'Success :)',
          'account Edited!',
          [
            {
              text: 'Ok',
              onPress: () => {
                this.goToHome()
              }
            },
          ],
          {cancelable: false},
        )
      } else {
        Alert.alert(
          'Ops :(',
          'New user not created, please try again!',
          [
            {
              text: 'Ok'
            },
          ],
          {cancelable: false},
        )
      }
    }
  }

  goToLogin() {
    this.props.navigation.navigate('LoginScreen')
  }

  goToHome() {
    this.props.navigation.navigate('HomeScreen', {change: true})
  }

  logout() {
    this.props.logout()
    this.goToLogin()
  }

  getIconBackHeader() {
    return (
      <Icon
        name='ios-arrow-back'
        type='ionicon' color={appStyle.color11}
        onPress={() => {
          this.goToHome()
        }}/>
    )
  }

  getRightComponent() {
    return (
      <Icon
        name='ios-log-out'
        type='ionicon' color={appStyle.color11}
        onPress={() => {
          this.logout()
        }}
      />
    )
  }

  selecionarAvatar() {
    const options = {
      title: 'Select the image profile',
      storageOptions: {
        skipBackup: true,
        path: 'images',
        cameraRoll: true,
        waitUntilSaved: true
      },
    };

    ImagePicker.launchImageLibrary(options, (response) => {
      const source = {uri: 'data:' + response.type + ';base64,' + response.data};

      if (source.uri || source.uri !== '' ) {
        if (!source.uri.includes('undefined')) {
          this.setState({
            localUser: {...this.state.localUser, img: source}
          })
        }
      }
    })
  }

  render() {
    return (
      <Container style={appStyle.styles.container}>
        <Header
          backgroundColor={appStyle.color2}
          leftComponent={this.getIconBackHeader()}
          centerComponent={{text: translate('titleAccount'), style: {color: appStyle.color11, fontWeight: '600', fontSize: 26}}}
          rightComponent={this.getRightComponent()}
          containerStyle={[appStyle.styles.heightHeader]}
        />
        <SafeAreaView style={[appStyle.styles.bodyApp]}>

          <TouchableOpacity
            style={[appStyle.styles.viewAvatarEditCategory]}
            onPress={() => {
              this.selecionarAvatar()
            }}>
            {
              this.state.localUser.img === '' ?
                <Icon
                  name="ios-contact"
                  type={'ionicon'}
                  size={130}
                  color={appStyle.color11}
                  iconStyle={{backgroundColor: appStyle.styles.color10}}
                />
                :
                <Image
                  PlaceholderContent={<ActivityIndicator/>}
                  source={this.state.localUser.img} style={[appStyle.styles.imageEditItem]}/>
            }
          </TouchableOpacity>

          <Input
            containerStyle={[appStyle.styles.marginVertical, appStyle.styles.inputAccount]}
            inputContainerStyle={[{borderBottomWidth: 0,}]}
            inputStyle={{paddingLeft: 10}}
            placeholder={translate('name')}
            onChangeText={(text) => {
              let localUser = this.state.localUser
              localUser.name = text
              this.setState({localUser})
            }}
            value={this.state.localUser.name}
            leftIcon={
              <Icon
                name="ios-contact"
                type={'ionicon'}
                size={24}
                color={appStyle.color4}
                iconStyle={{backgroundColor: appStyle.styles.color10}}
              />
            }
          />

          <Input
            containerStyle={[appStyle.styles.marginVertical, appStyle.styles.inputAccount]}
            inputContainerStyle={[{borderBottomWidth: 0,}]}
            inputStyle={{paddingLeft: 10}}
            placeholder={translate('email')}
            onChangeText={(text) => {
              let localUser = this.state.localUser
              localUser.email = text.toLowerCase()
              this.setState({localUser})
            }}
            value={this.state.localUser.email}
            leftIcon={
              <Icon
                name="ios-mail"
                type={'ionicon'}
                size={24}
                color={appStyle.color4}
                iconStyle={{backgroundColor: appStyle.styles.color10}}
              />
            }
          />

          <Input
            containerStyle={[appStyle.styles.marginVertical, appStyle.styles.inputAccount]}
            inputContainerStyle={{borderBottomWidth: 0,}}
            inputStyle={{paddingLeft: 10}}
            placeholder={translate('pass')}
            secureTextEntry={true}
            onChangeText={(text) => {
              let localUser = this.state.localUser
              localUser.password = text
              this.setState({localUser})
            }}
            value={this.state.localUser.password}
            leftIcon={
              <Icon
                name="md-key"
                type={'ionicon'}
                size={24}
                color={appStyle.color4}
                iconStyle={{backgroundColor: appStyle.styles.color10}}
              />
            }
          />

          <Button
            buttonStyle={[appStyle.styles.btnSave, appStyle.styles.marginBtnLogin]}
            titleStyle={[appStyle.styles.btnTitleSave]}
            title={translate('txtBtnEditImage')}
            onPress={
              () => {
                this.selecionarAvatar()
              }
            }
          />

          <Button
            buttonStyle={[appStyle.styles.btnSave, appStyle.styles.marginBtnLogin]}
            titleStyle={[appStyle.styles.btnTitleSave]}
            title={translate('txtBtnSave')}
            onPress={
              () => {
                this.accountValidation()
              }
            }
          />

        </SafeAreaView>
      </Container>
    )
  }
}

const mapStateToProps = state => ({
  user: state.users.filter((u) => u.logged)[0],
})

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout()),
  editUser: (user) => dispatch(editUser(user)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Account)
